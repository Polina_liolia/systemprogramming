﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _051_Async
{
    class Program
    {
        //делегат можно вызвать двумя способами 
        //1) .Invoke - в том же потоке
        //2) .BeginInvoke - две версии
        //-1)версия - два параметра - первый - делегат обратного вызова CallBack, второй это прараметр для CallBack (пример .BeginInvoke(null, null))
        //-2)версия, когда есть параметры для самого делегата,  
        // .EndInvoke используется только если нужно дожидаться окончания  работы BeginInvoke - аналог Thread.Join, Task.Wait
        

        static void Main()
        {
            var myDelegate = new Func<int, int, int>(Add);
           
            // Так как класс делегата сообщается с методами, которые принимают два целочисленных параметра, метод BeginInvoke также
            // начинает принимать два дополнительных параметра, кроме двух последних постоянных аргументов.
            IAsyncResult asyncResult = myDelegate.BeginInvoke(1, 2,
                                                               null,
                                                               null); //1-2 это параметры для функции Add
                                                                //предпоследний нулл это ссылка на CallBack
                                                                // последний нулл это парметры CallBack

            // Ожидание завершения асинхронной операции и получение результата работы метода.
            int result = myDelegate.EndInvoke(asyncResult);

            Console.WriteLine("Результат = " + result);

            // Delay.
            Console.ReadKey();
        }

        // Метод для выполнения в отдельном потоке.
        static int Add(int a, int b)
        {
            Thread.Sleep(2000);
            return a + b;
        }
    }
}
