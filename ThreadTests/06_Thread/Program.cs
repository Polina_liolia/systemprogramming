﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _06_Thread
{
    class Program
    {
        //Каждый поток работает в Домене приложений.

        //    Каждый поток принадлежит какому-то Домену приложений.
        //    Принадлежность может изменяться.
        //    Поток может общаться с несколькими доменами приложения.
       
        // Общая переменная счетчик (если без тега)
         // [ThreadStatic] //делает значение статического поля отдельным для каждого потока
        public static int counter;

        // Рекурсивный запуск потоков.
        public static void Method()
        {
            if (counter < 100)
            {
                counter++; // Увеличение счетчика вызваных методов.
                Console.WriteLine(counter + " - СТАРТ --- " + Thread.CurrentThread.GetHashCode());
                var thread = new Thread(Method);
                thread.Start();
                thread.Join(); // Закомментировать.               
            }

            Console.WriteLine("Поток {0} завершился.", Thread.CurrentThread.GetHashCode());
        }

        static void Main()
        {
            // Запуск вторичного потока.
            var thread = new Thread(Method);
            thread.Start();
            thread.Join();

            Console.WriteLine("Основной поток завершил работу...");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
