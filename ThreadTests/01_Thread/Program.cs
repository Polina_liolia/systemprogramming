﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_Thread
{
    class Program
    {
        // Метод выполяняющийся во Вторичном потоке.
        private static void ThreadFunc()
        {
            Console.WriteLine("ID Вторичного потока: {0}", Thread.CurrentThread.ManagedThreadId);
            Console.ForegroundColor = ConsoleColor.Yellow;

            for (int i = 0; i < 160; i++)
            {
                Thread.Sleep(20);
                Console.Write(".");
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Вторичный поток завершился.");
        }

        static void Main()
        {
            // Создание нового потока.
            var thread = new Thread(new ThreadStart(ThreadFunc));

            Console.WriteLine("ID Первичного потока: {0} \n", Thread.CurrentThread.GetHashCode());
            Console.WriteLine("Запуск нового потока...");

            thread.Start();
            Console.WriteLine(Thread.CurrentThread.GetHashCode());
            // Если необходимо ожидание первичным потоком завершения работы вторичного потока, вызывется метод Join():
             thread.Join(); //TODO Снять комментарий. 
            //Join - дождаться выполнение операции. 
            //Что будет, если главный поток завершит своё действие раньше:
            //Выполнение дочерних завершится без их уведомления самой OS.
            //У программиста внутри параллельного потока нет возможности узнать, что действие уже не нужно.

            //Без вызова Join() первичный и вторичный поток будут работать параллельно.
            Console.ForegroundColor = ConsoleColor.Green;

            for (int i = 0; i < 160; i++)
            {
                Thread.Sleep(20);
                Console.Write("-");
            }

            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("\nПервичный поток завершился.");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
