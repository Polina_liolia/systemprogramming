﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _05_Async
{

    class Program
    {
        static void Main()
        {
            Console.WriteLine("Первичный поток: Id {0}", Thread.CurrentThread.ManagedThreadId);

            var myDelegate = new Action(Method);

            // BeginInvoke - выполняем метод Method в отдельном потоке, взятом из пула потоков.
            // IAsyncResult - представляет состояние асинхронной операции.
            IAsyncResult asyncResult = myDelegate.BeginInvoke(null, //callback 
                null //callback parametre
                );

            Console.WriteLine("Первичный поток продолжает работать.");
           
            myDelegate.EndInvoke(asyncResult); // Ожидание завершения асинхронной операции.

            Console.WriteLine("Первичный поток завершил работу.");

            // Delay.
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        // Метод для выполнения в отдельном потоке.
        static void Method()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nАсинхронный метод запущен.");
            Console.WriteLine("\nВторичный поток: Id {0}", Thread.CurrentThread.ManagedThreadId);

            for (int i = 0; i < 80; i++)
            {
                Thread.Sleep(50);
                Console.Write(".");
            }

            Console.WriteLine("Асинхронная операция завершена.\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
