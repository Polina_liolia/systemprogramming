﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using Scripting;

namespace ZippingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ArchivDB();
            CompressedZipFileFromDirectory(@"C:\TestRar2", @"C:\test.txt");
        }
        private static string expansion = ".ZIP";

        public static bool ArchivDB()
        {
            string PathToArchivDB = @"C:\TestRar"  ;//
            string NameArchiv = "testrar.rar";
            string PathToRar = "C:\\rar.exe";
            string NameDB = "rar.exe";

            FileSystemObject fso = new FileSystemObject();
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            // a D:\_Delphin\DB\ArchivData\TestRar\gl.rar D:\_Delphin\DB\ArchivData\TestRar\GL.GDB
            si.Arguments = " a " + PathToArchivDB + "\\" + NameArchiv + " " + PathToArchivDB + "\\" + NameDB;
            si.FileName = PathToRar;
            si.UseShellExecute = false;
            si.CreateNoWindow = true;

            System.Diagnostics.Process pr = System.Diagnostics.Process.Start(si);
            pr.WaitForExit();

            return true;


        }
        //--------------------------------------
        private static bool CreateZipCopyFile(string nameDirectoryToCopy, string fileName)
        {
            bool result = true;

            if (!CompressedZipFileFromDirectory(nameDirectoryToCopy, fileName))
                result = false;

            return result;
        }
        //--------------------------------------
        //-------------------------------
        public static bool CompressedZipFileFromDirectory(string namePathForCompressed, string nameFile)
        {
            bool result = true;

            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    /*
                    String[] pathNames = System.IO.Directory.GetDirectories(namePathForCompressed);
                    foreach (string pathName in pathNames)
                    {
                        LoggerInspector.Add("Adding {0}...  " + pathName);
                        
                        ZipEntry ePath = zip.AddDirectory(pathName.Substring(pathName.LastIndexOf('\\')));
                        ePath.Comment = "Added by Cheeso's CreateZip utility.";
                        // note: this does not recurse directories! 
                        String[] filenames = System.IO.Directory.GetFiles(pathName);

                        // This is just a sample, provided to illustrate the DotNetZip interface.  
                        // This logic does not recurse through sub-directories.
                        // If you are zipping up a directory, you may want to see the AddDirectory() method, 
                        // which operates recursively. 
                        foreach (String filename in filenames)
                        {
                            LoggerInspector.Add("Adding {0}...  " + filename);
                            ZipEntry e = zip.AddFile(filename);
                            e.Comment = "Added by Cheeso's CreateZip utility.";
                        }

                        zip.Comment = String.Format("This zip archive was created by the CreateZip example application on machine '{0}'",
                           System.Net.Dns.GetHostName());

                    }
                     */
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(namePathForCompressed); // recurses subdirectories
                    //zip.Save(ZipFileToCreate)

                    zip.Save(nameFile + expansion);
                }

            }
            catch (System.Exception ex1)
            {
                result = false;
                System.Console.Error.WriteLine("exception: " + ex1);
            }

            return result;
        }
        //-------------------------------

    }
}
