﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace MyWindowsService
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        public Installer()
        {
            this.Installers.Add(new ServiceProcessInstaller
            {
                Account = ServiceAccount.LocalSystem
            });
            this.Installers.Add(new ServiceInstaller
            {
                ServiceName = "===== TEST SERVICE =====",
                Description = "My First NT Service!",
                StartType = ServiceStartMode.Automatic
            });

        }
    }
}
