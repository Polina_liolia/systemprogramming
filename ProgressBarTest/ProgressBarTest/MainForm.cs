﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgressBarTest
{
    public partial class MainForm : Form
    {
        MyProgressBar pb;
        delegate void MyWorkFunctionDelegate();
        public MainForm()
        {
            InitializeComponent();
            pb = new MyProgressBar(this);
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            if (pb.state != ReportState.Canceled)
            {
                MyWorkFunctionDelegate workFunction = new MyWorkFunctionDelegate(MyWorkFunction);
                workFunction.BeginInvoke(null, null); //запуск из главного потока
                pb.ShowProgressForm();
            }
        }

        public void MyWorkFunction()
        {
            int n = 200000000;
            int progressCounter = 0;
            int Count = n / 200000;
            object sender = Thread.CurrentThread;
            ShowProgressArgs e = new ShowProgressArgs(Count, progressCounter);
            e.Caption = "";
            for (int i = 0; i < n; i++)
            {
                if (i%200000 == 0)
                {
                    progressCounter++;
                    e.Caption = progressCounter.ToString();
                    e.EventsSoFar = progressCounter;
                    pb.ShowProgress(sender, e);
                    if (e.Cancel)
                        return;
                }
            }
        }
    }
}
