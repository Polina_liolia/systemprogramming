﻿using ProgressBarTest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//

public class MyProgressBar
{
    Form mainForm;
    ProgressForm pf;
    public ReportState state;

    public MyProgressBar(Form form)
    {
        mainForm = form;
        state = ReportState.Pending;
    }

    public void ShowProgressForm()
    {
        pf = new ProgressForm(this);
        pf.ShowDialog();
    }
    // Делегат для 
    delegate void ShowProgressHandler(object sender, ShowProgressArgs e);
    // Делегат для уведомления UI-потока о прогрессе рабочего потока
    delegate void ShowProgressDelegate(int totalEvents, int eventsSoFar);

    public void ShowProgress(object sender, ShowProgressArgs e)
    {
        // Убедиться, что мы в нужном потоке
        if (mainForm.InvokeRequired == false)
        {
            if (String.IsNullOrEmpty(e.Caption))
            {
                pf.newValues(e.TotalEvents, e.EventsSoFar);
            }
            else
            {
                pf.newValues(e.TotalEvents, e.EventsSoFar, e.Caption);
            }

            // Проверить Cancel
            e.Cancel = (state == ReportState.Canceled);

            // Проверить завершение работы
            if (e.Cancel || (e.EventsSoFar == e.TotalEvents))
            {
                state = ReportState.Pending;
                pf.Close();
            }
        }
        // Передать управление нужному потоку
        else
        {
            ShowProgressHandler showProgress = new ShowProgressHandler(ShowProgress);
            mainForm.Invoke(showProgress, new object[] { sender, e }); //вызов должен происходить из потока, в котором был создан GUI (mainForm)
        }
    }


    
}

public enum ReportState
{
    Pending,     // подготовка отчета не выполняется и не отменяется
    Canceled,    // подготовка отчета отменена в UI-потоке, но не в рабочем
}

public class ShowProgressArgs : EventArgs
{
    public int TotalEvents;
    public int EventsSoFar;
    public bool Cancel;
    public string Caption;
    public ShowProgressArgs(int totalEvents, int eventsSoFar)
    {
        this.TotalEvents = totalEvents;
        this.EventsSoFar = eventsSoFar;
    }
}