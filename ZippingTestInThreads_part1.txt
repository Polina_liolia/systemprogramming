using System.Threading;
using System.Threading.Tasks;
using Ionic.Zip;
using Scripting;
//
#region ����������� ���������� "������"- �������������� ������ ����� ������, ����� ���������� ������ ������.
    class Program
    {
        // ����� ������� ����� �������� ��� ������ : �������� ������������� �����
        //��������� ���������� �� 09_TPL �� ThreadTests � �������, ������� ��������� ���� � ��� ����� ��� ���������
        static void MyTask()
        {
            Console.WriteLine("MyTask() �������.");

            CompressedZipFileFromDirectory(@"C:\TestRar2", @"C:\test.txt");

            Console.WriteLine("MyTask() ��������.");
        }
       
        // ����� ����������� ��� ����������� ������ : �������� ���������� ������ �� �����, ��� � �.�.
        static void ContinuationTask(Task task)
        {
            Console.WriteLine("����������� ��������.");
            FileSystemObject fso = new FileSystemObject();
            if (fso.FileExists(@"C:\test.txt.ZIP"))
            System.Windows.Forms.MessageBox.Show("����� ������ � ���������.");
            else
                System.Windows.Forms.MessageBox.Show("������ � �������� ������.");

            Console.WriteLine("����������� ���������.");
        }

        static void Main()
        {
            Console.WriteLine("�������� ����� �������.");

            // �������� ���������� ������.
            var action = new Action(MyTask);
            var task = new Task(action);

            // �������� ����������� ������.
            var continuationAction = new Action<Task>(ContinuationTask);
            Task taskContinuation = task.ContinueWith(ContinuationTask);

            // ���������� ������������������ �����.
            task.Start();

            // �������� ���������� �����������. 
            taskContinuation.Wait();
            //task.Wait();
            task.Dispose();
            taskContinuation.Dispose();

            Console.WriteLine("�������� ����� ��������.");

            // ��������.
            Console.WriteLine("������� ����� �������...");
            Console.ReadKey();
        }
    }
    #endregion