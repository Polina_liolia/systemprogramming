﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncDownLoad_GUI_Problem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GetButtonClick(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Beginning download\n";
            var req = (HttpWebRequest)WebRequest.Create("http://www.google.com");
            req.Method = "GET";
            req.BeginGetResponse(
                asyncResult =>
                {
                    var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
                    string headersText = resp.Headers.ToString();
                    //dataTextBox.Text += headersText; //exception - calling the interface component, that was created in the main thread;
                                                     //current thread has no access to the components of main thread
                    #region GUI Update WinForm Style
                    //MainWindow myForm = this;
                    //myForm.Invoke(new EventHandler(delegate
                    //{
                    //    dataTextBox.Text += headersText;
                    //}));
                    #endregion
                },
                null);
            dataTextBox.Text += "Download started async\n";
        }

        //problem solving:
        #region GUI Update
        private void GetButtonClick2(object sender, RoutedEventArgs e)
        {
            dataTextBox.Text += "Beginning download\n";
            var sync = SynchronizationContext.Current;
            var req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            req.Method = "GET";
            req.BeginGetResponse(
                asyncResult =>
                {
                    var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);
                    string headersText = resp.Headers.ToString();
                    sync.Post(
                        state => dataTextBox.Text += headersText,
                        null);
                },
                null);
            dataTextBox.Text += "Download started async\n";
        }
        #endregion
    }
}
