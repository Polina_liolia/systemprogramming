﻿namespace ParallelLoopsInWindowsFormsDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnStartParallelTask = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(15, 80);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(257, 169);
            this.txtOutput.TabIndex = 0;
            // 
            // btnStartParallelTask
            // 
            this.btnStartParallelTask.Location = new System.Drawing.Point(15, 25);
            this.btnStartParallelTask.Name = "btnStartParallelTask";
            this.btnStartParallelTask.Size = new System.Drawing.Size(75, 23);
            this.btnStartParallelTask.TabIndex = 1;
            this.btnStartParallelTask.Text = "Start!";
            this.btnStartParallelTask.UseVisualStyleBackColor = true;
            this.btnStartParallelTask.Click += new System.EventHandler(this.btnStartParallelTask_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnStartParallelTask);
            this.Controls.Add(this.txtOutput);
            this.Name = "MainForm";
            this.Text = "Главное Окно";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnStartParallelTask;
    }
}

