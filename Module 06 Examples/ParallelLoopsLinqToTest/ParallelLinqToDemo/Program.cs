﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParallelLinqToDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            const int count = 50;

            // let's generate an array of numbers from 1 to 20
            var items = Enumerable.Range(1, count).ToArray();

            // now we can get the cubed value of each element in the array using
            var results = new int[count];
            items.AsParallel().ForAll(x =>
            {
                int newValue = x * x * x;
                Console.Write($"{newValue} ({Task.CurrentId})\t");
                results[x - 1] = newValue;
            });
            Console.WriteLine();
            Console.WriteLine();

            foreach (var i in results)
                Console.Write($"{i}\t");
            Console.WriteLine();

            // now let's get an enumeration
            // by default, the sequence is quite different to our nicely laid out array
            // but....                    .AsOrdered()
            // also...                    .AsUnordered()
            var cubes = items.AsParallel().AsOrdered().Select(x => x * x * x);

            // this evaluation is delayed: you actually calculate the values only
            // when iterating or doing ToArray()

            foreach (var i in cubes)
                Console.Write($"{i}\t");
            Console.WriteLine();
            //
            #region some operations in LINQ perform an aggregation
            //var sum = Enumerable.Range(1, 1000).Sum();
            //var sum = ParallelEnumerable.Range(1, 1000).Sum();


            // Sum is just a specialized case of Aggregate
            //var sum = Enumerable.Range(1, 1000).Aggregate(0, (i, acc) => i + acc);

            var sum = ParallelEnumerable.Range(1, 1000)
              .Aggregate(
                  0, // initial seed for calculations
                  (partialSum, i) => partialSum += i, // per task
                  (total, subtotal) => total += subtotal, // combine all tasks
                  i => i); // final result processing

            Console.WriteLine($"Sum is {sum}");
            #endregion
        }
    }
}
