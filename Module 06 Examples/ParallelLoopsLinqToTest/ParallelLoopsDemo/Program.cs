﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelLoopsDemo
{
    class Program
    {
        public static IEnumerable<int> Range(int start, int end, int step)
        {
            for (int i = start; i < end; i += step)
            {
                yield return i;
            }
        }

        //класс Parallel применяется, чтобы распараллелить действия в циклах, 
        //т.е.все итерации будут выполняться не последовательно, а параллельно.
        //Примеры для статических методов For() и ForEach()
        #region 
    static void Main(string[] args)
    {
        var a = new Action(() => Console.WriteLine($"First {Task.CurrentId}"));
        var b = new Action(() => Console.WriteLine($"Second {Task.CurrentId}"));
        var c = new Action(() => Console.WriteLine($"Third {Task.CurrentId}"));
        Console.WriteLine("Parallel.Invoke");
        Parallel.Invoke(a, b, c);
        // these are blocking operations; wait on all tasks

        Parallel.For(1, 11, x =>
        {
            Console.Write($"{x * x}\t");
        });
        Console.WriteLine();
        Console.WriteLine("Parallel.ForEach");
        string[] words = { "oh", "what", "a", "night" };

        Parallel.ForEach(words, word =>
        {
            Console.WriteLine($"{word} has length {word.Length} (task {Task.CurrentId})");
        });
        Console.WriteLine();
        Console.WriteLine("Parallel.ForEach 2");

        var po = new ParallelOptions();
        po.MaxDegreeOfParallelism = 2;
        Parallel.ForEach(words, po, word =>
        {
            Console.WriteLine($"{word} has length {word.Length} (task {Task.CurrentId})");
        });

        Console.WriteLine("Parallel.ForEach && Range");
        // has a step strictly equal to 1
        // if you want something else...
        Parallel.ForEach(Range(1, 20, 3), Console.WriteLine);

        Console.WriteLine("Press any key ...");
        Console.ReadKey();
    }
        #endregion
        #region Долгий цикл
        /*
    static void Main(string[] args)
    {
        long end = 100000;
        double[] massiv = new double[end + 1];
        {
            Console.WriteLine("Начало очень долгой операции ...");
            DateTime dt_start = DateTime.Now;
            string buf = "";
            // Используем стандартный цикл
            for (long i = 0; i <= end; i++)
            {
                buf += i.ToString();
            }
            Console.WriteLine((DateTime.Now - dt_start).Seconds.ToString());
            // У меня было 23 секунд
            Console.WriteLine("Press any key ...");
            Console.ReadLine();
        }
        {
            DateTime dt_start = DateTime.Now;
            string buf = "";
            // Используем стандартный цикл
            Parallel.For(0, end + 1, index => { buf += index; });
            // Здесь было 3 секунды
            Console.WriteLine((DateTime.Now - dt_start).Seconds.ToString());
            Console.WriteLine("Press any key ...");
            Console.ReadLine();
        }
    }
    */
        #endregion
    }
}
