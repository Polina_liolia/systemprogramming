public static bool CompressedZipFileFromDirectory(string namePathForCompressed, string nameFile)
        {
            bool result = true;

            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(namePathForCompressed); // recurses subdirectories
                    zip.TempFileFolder = @"c:\tmpinfo";//Environment.CurrentDirectory;// @"c:\tmpinfo";//
                    zip.Save(nameFile + ".ZIP");
                }

            }
            catch (System.Exception ex1)
            {
                result = false;
                System.Console.Error.WriteLine("exception: " + ex1);
            }

            return result;
        }