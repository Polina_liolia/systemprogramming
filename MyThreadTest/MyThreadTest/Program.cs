﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyThreadTest
{
    class Program
    {
        public static int a = 0;
        static void Main(string[] args)
        {
            #region Thread without params
            Thread myThread = new Thread(new ThreadStart(Count));
            //Thread myThread = new Thread(Count); //the same as pevious code
            myThread.Start();

            for (int i = 0; i < 9; i++)
            {
                Console.WriteLine($"Main thead: {a++}");
            }
            Console.WriteLine();
            #endregion

            #region ParameterizedThread with one param
            int number = 4;
            // создаем новый поток
            myThread = new Thread(new ParameterizedThreadStart(Count));
            myThread.Start(number);

            for (int i = 1; i < 9; i++)
            {
                Console.WriteLine($"Главный поток: {i * i}");
                Thread.Sleep(300);
            }
            #endregion

            #region ParameterizedThread with many params
            Counter counter = new Counter();
            counter.x = 4;
            counter.y = 5;

            myThread = new Thread(new ParameterizedThreadStart(Count));
            myThread.Start(counter);
            #endregion

            #region ParameterizedThread with many params inside class
            Counter_ counter_ = new Counter_(5, 4);

            myThread = new Thread(new ThreadStart(counter_.Count));
            myThread.Start();
            #endregion

        }

        public static void Count()
        {
            Console.WriteLine($"Thread 2: {a++}");
        }

        public static void Count(object x)
        {
            for (int i = 1; i < 9; i++)
            {
                int n = (int)x;
                Console.WriteLine($"Второй поток: {i * n}");
                Thread.Sleep(400);
            }
        }

        public static void CountManyParams(object obj)
        {
            for (int i = 1; i < 9; i++)
            {
                Counter c = (Counter)obj;

                Console.WriteLine("Второй поток:");
                Console.WriteLine(i * c.x * c.y);
            }
        }

        public class Counter
        {
            public int x;
            public int y;
        }

        public class Counter_
        {
            private int x;
            private int y;

            public Counter_(int _x, int _y)
            {
                this.x = _x;
                this.y = _y;
            }

            public void Count()
            {
                for (int i = 1; i < 9; i++)
                {
                    Console.WriteLine("Второй поток:");
                    Console.WriteLine(i * x * y);
                    Thread.Sleep(400);
                }
            }
        }
    }
}
