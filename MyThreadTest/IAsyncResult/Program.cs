﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IAsyncResult_
{
    public delegate int BinaryOp(int data, int time);

    class Program
    {
        static void Main()
        {
            Console.WriteLine("Синхронный вызов метода: \n");
            // Синхронный вызов метода
            DelegateThread(1, 5000);

            Console.WriteLine("\nАсинхронный вызов метода с двумя потоками: \n");
            // Асинхронный вызов метода с применением делегата
            BinaryOp bo = DelegateThread;

            IAsyncResult ar = bo.BeginInvoke(1, 5000,//аргументы метода, перенданного делегату
                null,//callback
                null);//object - callback param
            while (!ar.IsCompleted)
            {
                // Выполнение операций в главном потоке
                Console.Write(".");
                Thread.Sleep(50);
            }
            int result = bo.EndInvoke(ar);
            Console.WriteLine("Result: " + result);

            Console.ReadLine();
        }

        static int DelegateThread(int data, int time)
        {
            Console.WriteLine("DelegateThread запущен");
            // Делаем задержку, для эмуляции длительной операции
            Thread.Sleep(time);
            Console.WriteLine("DelegateThread завершен");
            return ++data;
        }
    }
}
