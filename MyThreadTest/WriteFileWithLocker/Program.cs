﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WriteFileWithLocker
{
    class Program
    {
        static void Main(string[] args)
        {
            FileWriter fw1 = new FileWriter("data.txt", "Здесь был Task 1", 3);
            Task task1 = new Task(fw1.Write);
            task1.Start();
            task1.Wait();

            FileWriter fw2 = new FileWriter("data.txt", "Здесь был Task 2", 3);
            Task task2 = new Task(fw2.Write);
            task2.Start();
            task2.Wait();

            FileWriter fw3 = new FileWriter("data.txt", "Здесь был Task 3", 3);
            Task task3 = new Task(fw3.Write);
            task3.Start();
            task3.Wait();
        }

        public class FileWriter
        {
            private static Random rand;
            private static object locker;
            public string Path { get; set; }
            public string Data { get; set; }
            public int Count { get; set; }
            public FileWriter(string path, string data, int count)
            {
                Path = path;
                Data = data;
                Count = count;
            }
            static FileWriter()
            {
                locker = new object();
                rand = new Random();
            }
            public void Write()
            {
                Console.WriteLine($"Task {Task.CurrentId} start");
                for(int i = 0; i < Count; i++)
                {
                    lock(locker)
                    {
                        using (StreamWriter writer = new StreamWriter(Path, true))
                        {
                            writer.WriteLine(Data);
                            writer.Close();
                        }
                    }
                    Task.Delay(rand.Next(100, 700));
                }
                Console.WriteLine($"Task {Task.CurrentId} end");
            }
        }
    }
}
