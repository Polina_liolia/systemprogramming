﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoResetEvent_
{
    class Program
    {
        static AutoResetEvent waitHandler = new AutoResetEvent(true); //true-создаваемый объект изначально будет в сигнальном состоянии.
        static int x = 0;

        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                Thread myThread = new Thread(Count);
                myThread.Name = "Поток " + i.ToString();
                myThread.Start();
            }

            Console.ReadLine();
        }
        public static void Count()
        {
            waitHandler.WaitOne(); // текущий поток переводится в состояние ожидания, пока объект waitHandler не будет переведен в сигнальное состояние
            //то же самое, но такой синтаксис используетс для перевода сразу нескольких объектов AutoResetEvent:
            // AutoResetEvent.WaitAll(new WaitHandle[] { waitHandler }); 
            x = 1;
            for (int i = 1; i < 9; i++)
            {
                Console.WriteLine("{0}: {1}", Thread.CurrentThread.Name, x);
                x++;
                Thread.Sleep(100);
            }
            waitHandler.Set();/*уведомляет все ожидающие потоки, что объект waitHandler снова находится в сигнальном состоянии,
            и один из потоков "захватывает" данный объект, переводит в несигнальное состояние и выполняет свой код. 
            А остальные потоки снова ожидают.*/
        }
    }
}
