﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskBasedAsynchronousPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //запускаем асинхронный метод, внутрои которого, в свою очеридь, будет так же запущена на выполнение задача
            Task task = DisplayResultAsync();
            task.Wait();//Чтобы главный поток в обязательном порядке дождался окончания выполнения
            //DisplayResultAsync().GetAwaiter().GetResult(); //аналог task.Wait();
            Console.WriteLine("Задача DisplayResultAsync завершена");
            Console.ReadLine();
        }

        static async Task DisplayResultAsync()
        {
            Console.WriteLine("DisplayResultAsync start");
            int num = 5;
            //await - выполнение в вызывающем потоке останавливается, пока не завершится вызываемая задача
            //т.е. подождать, пока закончит работать этот метод, т.к. он содержит в себе запуск задачи
            int result = await FactorialAsync(num); 
            Thread.Sleep(3000);
            Console.WriteLine("Факториал числа {0} равен {1}", num, result);
            Console.WriteLine("DisplayResultAsync end");
        }

        static Task<int> FactorialAsync(int x)
        {
            Console.WriteLine("FactorialAsync start");
            int result = 1;

            return Task.Run(() =>
            {
                Console.WriteLine("FactorialAsync Task start");
                for (int i = 1; i <= x; i++)
                {
                    result *= i;
                }
                Console.WriteLine("FactorialAsync Task end");
                return result;
            });
            
        }
    }
}
