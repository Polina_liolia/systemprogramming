﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcurrentDictionary_
{
    class Program
    {
        static ConcurrentDictionary<int, string> myDictionary = new ConcurrentDictionary<int, string>();
        static void Main(string[] args)
        {  
            List<string> firstList = new List<string>()
            {
                "zero", "one", "two", "three",  "four", "five", "six", "seven",
                "eight",  "nine"
            };
            List<string> secondList = new List<string>()
            {
                "ten", "eleven", "twelw", "thirteen", "fourteen", "fifteen", "sixteen",
                "seventeen", "eighteen", "nineteen"
            };
            List<string> thirdList = new List<string>()
            {
               "twenty", "twenty one", "twenty two", "twenty three",  "twenty four",
                "twenty five", "twenty six", "twenty seven", "twenty eight",  "twenty nine"
            };

            Task t1 = new Task(() => addItems(firstList, 0));
            Task t2 = new Task(() => addItems(secondList, 10));
            Task t3 = new Task(() => addItems(thirdList, 20));
            Task t_remover = new Task(() => removeItems(0, 30));
          
            t1.Start();
            t2.Start();
            t3.Start();
            t_remover.Start();
            Task.WaitAll(t1, t2, t3, t_remover);


        }
        public static void addItems (List<string> list, int additionalNumber)
        {
            Random rand = new Random();
            for (int i = 0; i < list.Count; i++)
            {
                bool added = false;
                int efforts = 0;                
                while (!added && efforts != 5)
                {
                    added = myDictionary.TryAdd(i + additionalNumber, list[i]);//Этот метод возвращает false Если ключ уже существует. 
                    //Используйте TryUpdate или AddOrUpdate метод для обновления значения, в случае, если ключ уже существует.
                    if (added)
                        Console.WriteLine($"{list[i]} added");
                    else
                        Console.WriteLine($"{list[i]} adding failed (effort {efforts})");
                    efforts++;
                }
                    
                if (rand.Next(0, 1) == 1)
                    Task.Delay(rand.Next(1000, 2000));
            }
        }

        public static void removeItems(int start, int end)
        {
            Random rand = new Random();
            for (int i = start; i < end; i++)
            {
                string removedVal = string.Empty;
                bool removed = false;
                int efforts = 0;
                while (!removed && efforts != 5)
                {
                    removed = myDictionary.TryRemove(i, out removedVal);
                    if (removed)
                        Console.WriteLine($"{removedVal} removed");
                    else
                        Console.WriteLine($"{removedVal} removing failed (effort {efforts})");

                    efforts++;
                }
                if (rand.Next(0, 1) == 1)
                    Task.Delay(rand.Next(1000, 2000));
            }
        }

    }
}
