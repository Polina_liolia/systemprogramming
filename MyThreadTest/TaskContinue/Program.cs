﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread start");
            Random random = new Random();
            Task[] tasks = new Task[] 
            {
                new Task(() => PrintDot(random.Next(5, 15))),
                 new Task(() => PrintDot(random.Next(5, 15))),
                  new Task(() => PrintDot(random.Next(5, 15))),
                   new Task(() => PrintDot(random.Next(5, 15))),
                    new Task(() => PrintDot(random.Next(5, 15)))
            };

            foreach (Task t in tasks)
                t.Start();
           // Task.WaitAny(tasks);
            Task.WaitAll(tasks);
            Console.WriteLine("Main thread end");
        }

        public static void PrintDot(int number)
        {
            Console.WriteLine($"Task {Task.CurrentId} start");
            for (int i = 0; i < number; i++)
            {
                Console.Write(". ");
                Thread.Sleep(100);
            }
            Console.WriteLine($"Task {Task.CurrentId} end");
        }
    }
}
