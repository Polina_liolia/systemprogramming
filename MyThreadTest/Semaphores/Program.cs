﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Semaphores
{
    //Семафоры позволяют ограничить доступ определенным количеством объектов.

    /*
     есть некоторое число читателей, которые приходят в библиотеку три раза в день и что-то там читают. 
     И пусть у нас будет ограничение, что единовременно в библиотеке не может находиться больше трех читателей. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 6; i++)
            {
                Reader reader = new Reader(i);
            }

            Console.ReadLine();
        }
    }

    class Reader
    {
        // создаем семафор
        static Semaphore sem = new Semaphore(3, //какому числу объектов изначально будет доступен семафор
            3//какоe максимальное число объектов будет использовать данный семафор
            );
        Thread myThread;
        int count = 3;// счетчик чтения

        public Reader(int i)
        {
            myThread = new Thread(Read); //передаем в поток основной метод читателя - Read
            myThread.Name = "Читатель " + i.ToString(); //именуем поток
            myThread.Start(); //запускаем метод потока в работу
        }

        public void Read()
        {
            while (count > 0)
            {
                sem.WaitOne(); //запуск ожидания получения семафора

                Console.WriteLine("{0} входит в библиотеку", Thread.CurrentThread.Name);

                Console.WriteLine("{0} читает", Thread.CurrentThread.Name);
                Thread.Sleep(1000);

                Console.WriteLine("{0} покидает библиотеку", Thread.CurrentThread.Name);

                sem.Release(); //высвобождаем семафор;  в семафоре освобождается одно место, которое заполняет другой поток.

                count--;
                Thread.Sleep(1000);
            }
        }
    }
}
