﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockingCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            // A bounded collection. It can hold no more 
            // than 100 items at once.
            BlockingCollection<int> dataItems = new BlockingCollection<int>(100);


            // A simple blocking consumer with no cancellation.
            Task task_consumer = Task.Run(() =>
            {
                Random rand = new Random();
                while (!dataItems.IsCompleted)
                {
                    // Blocks if number.Count == 0
                    // IOE means that Take() was called on a completed collection.
                    int data = rand.Next(0, 20);
                    
                    // Some other thread can call CompleteAdding after we pass the
                    // IsCompleted check but before we call Take. 
                    // In this example, we can simply catch the exception since the 
                    // loop will break on the next iteration.
                    try
                    {
                        data = dataItems.Take();
                    }
                    catch (InvalidOperationException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    Console.WriteLine($"-{data}");
                   
                }
                Console.WriteLine("\r\nNo more items to take.");
            });

            // A simple blocking producer with no cancellation.
            Task task_producer = Task.Run(() =>
            {
                Random rand = new Random();
                int moreItemsToAdd = 10;
                while (moreItemsToAdd > 0 )
                {
                    int data = rand.Next(0, 20);
                    // Blocks if numbers.Count == dataItems.BoundedCapacity
                    dataItems.Add(data);
                    moreItemsToAdd--;
                    Console.WriteLine($"+{data}");
                    Task.Delay(5000);
                }
                // Let consumer know we are done.
                dataItems.CompleteAdding();
            });

            Task.WaitAll(task_consumer, task_producer);

        }
    }
}
