﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TasksContinue
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread starts work");
            Task<int> t = new Task<int>(() => Count(12, 15));
            t.ContinueWith(new Action<Task<int>>(TaskEnded));
            t.Start();
            //  Console.WriteLine($"Thread result: {t.Result}"); 
            t.Wait();
            Console.WriteLine("Main thread ends work");
           
        }

        public static int Count(int a, int b)
        {
            Console.WriteLine($"Task starts: {Task.CurrentId}");
            int result = 0;
            for (int i = a; i < b; i++)
            {
                result += i;
                Thread.Sleep(500);
            }
            Console.WriteLine($"Task {Task.CurrentId} had done ints work");
            return result;
        }

        public static void TaskEnded(Task<int> task)
        {
            Console.WriteLine($"Task {Task.CurrentId} ended work with result {task.Result}");
          
        }
    }
}
