﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreeThreadsFileWriters
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"../../../data.txt";

            #region Using threads
            DataWriter dw1 = new DataWriter(path, 1, "1");
            Thread thread1 = new Thread(new ThreadStart(dw1.WriteDataForThread));
            thread1.Name = "Thread 1";
            thread1.Start();
            thread1.Join();

            DataWriter dw2 = new DataWriter(path, 2, "2");
            Thread thread2 = new Thread(new ThreadStart(dw2.WriteDataForThread));
            thread2.Name = "Thread 2";
            thread2.Start();
            thread2.Join();

            DataWriter dw3 = new DataWriter(path, 3, "3");
            Thread thread3 = new Thread(new ThreadStart(dw3.WriteDataForThread));
            thread3.Name = "Thread 3";
            thread3.Start();
            thread3.Join();
            #endregion

            #region Using tasks
            Task task1 = new Task(dw1.WriteDataForTask);
            task1.Start();
            task1.Wait();
            Task task2 = new Task(dw2.WriteDataForTask);
            task2.Start();
            task2.Wait();
            Task task3 = new Task(dw3.WriteDataForTask);
            task3.Start();
            task3.Wait();
            #endregion

            #region IAsyncResult
            Action actionWriteFile = new Action(dw1.WriteData);
            IAsyncResult result1 = actionWriteFile.BeginInvoke(null, null);
            #endregion

        }
    }
}
