﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreeThreadsFileWriters
{
    public class DataWriter
    {
        private static object locker = new object();
        public string Path { get; set; }
        public int LineNumber { get; set; }
        public string Data { get; set; }
        public DataWriter(string path, int line_number, string data)
        {
            Path = path;
            LineNumber = line_number;
            Data = data;
        }

        public void WriteDataForThread()
        {
            lock(locker)
            {
                Console.WriteLine($"Thread: {Thread.CurrentThread.Name}");
                WriteData();
                Console.WriteLine($"{Thread.CurrentThread.Name} ended");
            }
        }

         async public void WriteDataForTask()
        {
            
            Console.WriteLine($"Task: {Task.CurrentId}");
            await Task.Run(() =>
            {
                string[] strings = new string[3];
                using (StreamReader reader = new StreamReader(new FileStream(Path, FileMode.Open), Encoding.Default))
                {
                    int i = 0;
                    while (!reader.EndOfStream)
                    {
                        strings[i++] = reader.ReadLine();
                    }
                }

                FileStream file = new FileStream(Path, FileMode.Open);
                int lineNumber = LineNumber - 1;
                strings[lineNumber] = strings[lineNumber].Insert(strings[lineNumber].Length, Data);
                file.Seek(0, SeekOrigin.Begin);

                using (StreamWriter writer = new StreamWriter(file, Encoding.Default))
                {
                    foreach (string s in strings)
                        writer.WriteLine(s);
                    writer.Close();
                }
            });
            Console.WriteLine($"{Task.CurrentId} ended");
            
        }

        public void WriteData()
        {
            string[] strings = new string[3];
            using (StreamReader reader = new StreamReader(new FileStream(Path, FileMode.Open), Encoding.Default))
            {
                int i = 0;
                while (!reader.EndOfStream)
                {
                    strings[i++] = reader.ReadLine();
                }
            }

            FileStream file = new FileStream(Path, FileMode.Open);
            int lineNumber = LineNumber - 1;
            strings[lineNumber] = strings[lineNumber].Insert(strings[lineNumber].Length, Data);
            file.Seek(0, SeekOrigin.Begin);

            using (StreamWriter writer = new StreamWriter(file, Encoding.Default))
            {
                foreach (string s in strings)
                    writer.WriteLine(s);
                writer.Close();
            }
        }
    }
}
