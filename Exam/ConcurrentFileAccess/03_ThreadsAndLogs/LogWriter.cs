﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LoggerLib;

namespace _03_ThreadsAndLogs
{
    public class LogWriter
    {
        #region Private fields
        private int maxBufferSize;
        private CancellationTokenSource cts;
        private FileStream stream;
        private object locker;
        private MyTxtLogger logger;
        #endregion
        #region Public properties
        public string PathPrefix { get; set; }
        public int LogsFrequencyMilliseconds { get; set; }
        public string CurrentPath { get; set; }
        #endregion
        #region Constructors
        private LogWriter() { }
        public LogWriter(int logsFrequencyMilliseconds = 10000, string pathPrefix = "")
        {
            logger = MyTxtLogger.CreateTxtLogger(false);
            logger.Use_D = true;
            logger.WriteProtocol("Start", "LogWriter", $"TaskID: {Task.CurrentId}");
            PathPrefix = pathPrefix;
            LogsFrequencyMilliseconds = logsFrequencyMilliseconds;
            maxBufferSize = GetMaxBuferSize(logsFrequencyMilliseconds);
            CurrentPath = GetCurrentFilePath();//initial value
            if (File.Exists(CurrentPath))
                File.Delete(CurrentPath);
            cts = new CancellationTokenSource();
            stream = null;
            locker = new object();
            CreateNewLogFile();
            logger.WriteProtocol("End", "LogWriter", $"TaskID: {Task.CurrentId}");
        }
        #endregion
        #region Private helpers
        private int GetMaxBuferSize(int logsFrequencyMilliseconds)
        {
            int maxBufferSize = (
                (60 * 60) //seconds in hour
                / (logsFrequencyMilliseconds / 1000) //frequency in seconds
                ); //maximum times of launching log writer in an hour 
            logger.WriteProtocol("Invoked", $"GetMaxBuferSize returned size {maxBufferSize}", $"TaskID: {Task.CurrentId}");
            return maxBufferSize;
        }

        private string GetCurrentFilePath()
        {
            logger.WriteProtocol("Start", "GetCurrentFilePath", $"TaskID: {Task.CurrentId}");
            DateTime dt = DateTime.Now;
            int year = dt.Year;
            int month = dt.Month;
            int day = dt.Day;
            int hour = dt.Hour;
            string path = $"{PathPrefix}output_{year}_{month}_{day}_{hour}.txt";
            logger.WriteProtocol("End", $"GetCurrentFilePath returned '{path}'", $"TaskID: {Task.CurrentId}");
            return path;
        }

        private bool CreateNewLogFile()
        {
            logger.WriteProtocol("Start", "CreateNewLogFile", $"TaskID: {Task.CurrentId}");
            bool created = false;
            lock (locker)
            {
                string path = GetCurrentFilePath();
                if (!File.Exists(path))
                {
                    File.Create(path).Close();

                    try
                    {
                        //closing and cleaning up previous connections if needed:
                        if (stream != null)
                        {
                            stream.Close();
                            stream.Dispose();
                        }
                        //creating new connections:
                       
                        using (StreamWriter writer = new StreamWriter(CurrentPath, false, Encoding.Default))
                        {
                            //writing initial buffers to new file:
                            string str = new string(' ', maxBufferSize);
                            for (int i = 0; i < 3; i++)
                            {
                                writer.WriteLine(str);
                            }
                        }
                        stream = new FileStream(path, FileMode.Open);
                        //indicating success:
                        created = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"CreateNewLogFile: {ex.Message}");
                    }
                }
            }
            logger.WriteProtocol("End", $"CreateNewLogFile result: {created}", $"TaskID: {Task.CurrentId}");
            return created;
        }

        private long GetLineStartPosition(int lineNumber)
        {
            long lineStart = ((maxBufferSize + 2) * (lineNumber - 1));
            logger.WriteProtocol("Invoked", $"GetLineStartPosition returned: {lineStart} for line {lineNumber}", $"TaskID: {Task.CurrentId}");
            return lineStart;
        }
        #endregion
        #region Write to file methods
        public void Start(int lineNumber, char value)
        {
            logger.WriteProtocol("Start", $"Start, line number {lineNumber}, value {value}", $"TaskID: {Task.CurrentId}");
            Task fileWriterTask = StartFileWriterTimerAsync(lineNumber, value);
            logger.WriteProtocol("End", $"Start, line number {lineNumber}, value {value}", $"TaskID: {Task.CurrentId}");
        }

        private async Task StartFileWriterTimerAsync(int lineNumber, char value)
        {
            logger.WriteProtocol("Start", $"StartFileWriterTimerAsync, line number {lineNumber}, value {value}", $"TaskID: {Task.CurrentId}");
            long cursorPosition = GetLineStartPosition(lineNumber);
            while (!cts.Token.IsCancellationRequested)
            {
                await Task.Delay(LogsFrequencyMilliseconds, cts.Token);
                if (!cts.Token.IsCancellationRequested)
                    cursorPosition = await WriteToFile(lineNumber, value, cursorPosition);
            }
            Console.WriteLine("File writer canceled");
            logger.WriteProtocol("End", $"StartFileWriterTimerAsync, line number {lineNumber}, value {value}", $"TaskID: {Task.CurrentId}");
        }

        private async Task<long> WriteToFile(int lineNumber, char value, long previous_position)
        {
            logger.WriteProtocol("Start", $"WriteToFile, line number {lineNumber}, value {value}, prev position {previous_position}", $"TaskID: {Task.CurrentId}");
            if (CurrentPath != GetCurrentFilePath())
            {
                CurrentPath = GetCurrentFilePath();
                bool created = CreateNewLogFile();
                if (created)
                    previous_position = GetLineStartPosition(lineNumber);
                else
                {
                    Console.WriteLine("Can't create new file");
                    cts.Cancel();
                    return -1;
                }
            }
            long newPosition = -1;
            try
            {
                lock (locker)
                {
                    stream.Seek(previous_position, SeekOrigin.Begin);
                    logger.WriteProtocol($"Seek: {previous_position}, stream position: {stream.Position}, stream length {stream.Length}", $"WriteToFile, line number {lineNumber}, value {value}, prev position {previous_position}", $"TaskID: {Task.CurrentId}");

                    stream.Write(Encoding.Default.GetBytes(new char[] { value }), 0, 1);
                }
                //writeTask.Wait();
                newPosition = previous_position + 1;
                Console.WriteLine($"Written '{value}' in {lineNumber} line on position {previous_position}, file: {CurrentPath}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"WriteToFile: {ex.Message}");
            }
            logger.WriteProtocol("End", $"WriteToFile, line number {lineNumber}, value {value}, prev position {previous_position}, returned new pos {newPosition}", $"TaskID: {Task.CurrentId}");
            return newPosition;
        }

        public void StopAllWriters()
        {
            logger.WriteProtocol("Start", $"StopAllWriters", $"TaskID: {Task.CurrentId}");
            cts.Cancel();
            stream.Close();
            stream.Dispose();
            logger.WriteProtocol("End", $"StopAllWriters", $"TaskID: {Task.CurrentId}");
        }
        #endregion
    }
}
