﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ThreadsAndLogs
{
    class Program
    {
        static void Main(string[] args)
        {
            LogWriter logWriter = new LogWriter(pathPrefix: "../../../");

            //creating and starting task to read console line and to set CancellationTokenSource as cancelled
            //after 'c' user's input
            Task cancellationTask = Task.Factory.StartNew(() =>
            {
                char character = Console.ReadKey().KeyChar;
                if (character.Equals('c'))
                    logWriter.StopAllWriters();
            });
            
            logWriter.Start(1, '1');
            logWriter.Start(2, '2');
            logWriter.Start(3, '3');
            
            cancellationTask.Wait();
            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }
    }
}
