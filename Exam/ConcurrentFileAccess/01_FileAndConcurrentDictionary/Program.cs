﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_FileAndConcurrentDictionary
{
    class Program
    {
        public static Random rand = new Random();
        //creating cancellation token source to be able to cancel threads work any time:
        public static CancellationTokenSource cts = new CancellationTokenSource();

        static void Main(string[] args)
        {
            ConcurrentDictionary<string, BlockingCollection<int>> dictionary = new ConcurrentDictionary<string, BlockingCollection<int>>();
            dictionary.TryAdd("Thread1", new BlockingCollection<int>(new ConcurrentBag<int>()));
            dictionary.TryAdd("Thread2", new BlockingCollection<int>(new ConcurrentBag<int>()));
            dictionary.TryAdd("Thread3", new BlockingCollection<int>(new ConcurrentBag<int>()));

            //creating and starting task to read console line and to set CancellationTokenSource as cancelled
            //after 'c' user's input
            Task cancellationTask = Task.Factory.StartNew(() =>
            {
                char character = Console.ReadKey().KeyChar;
                if (character.Equals('c'))
                    cts.Cancel();
            });

            //creating and starting tasks to fill BlockingCollections in:
            Task task1 = Task.Factory.StartNew(() => FillBagIn(dictionary["Thread1"], 1, cts.Token));
            Task task2 = Task.Factory.StartNew(() => FillBagIn(dictionary["Thread2"], 2, cts.Token));
            Task task3 = Task.Factory.StartNew(() => FillBagIn(dictionary["Thread3"], 3, cts.Token));

            //creating an object, that encapsulates callback method for timer and its parameter (path to file)
            //to avoid passing wrong object type in callback:
            FileWriter fileWriter = new FileWriter(@"../../../output1.txt", dictionary);

            //starting writing to file task with 10 seconds delay:
            Task timerTask = StartFileWriterTimerAsync(fileWriter.WriteToFile, 10000, cts.Token);
            
            Task.WaitAll(cancellationTask, task1, task2, task3);//main thread waiting for other threads
            Console.WriteLine("Main ended");
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private static async Task StartFileWriterTimerAsync(Action action, int millisecondsDelay, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await Task.Delay(millisecondsDelay, token);
                if (!token.IsCancellationRequested)
                    action();
            }
            Console.WriteLine("File writer canceled");
        }

        public static void FillBagIn(BlockingCollection<int> bag, int value, CancellationToken cancellationToken)
        {
            while(true)
            {
                try
                {
                    bag.Add(value, cancellationToken);
                    Thread.Sleep(rand.Next(1000, 3000)); //making a slow task 
                    Console.WriteLine($"Task {Task.CurrentId}: +{value}");
                }
                catch(OperationCanceledException)
                {
                    Console.WriteLine($"Task {Task.CurrentId}: operation cancelled.");
                    break;
                }
            }
        }

       
    }
}
