﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_FileAndConcurrentDictionary
{
    public class FileWriter
    {
        private string path;
        private ConcurrentDictionary<string, BlockingCollection<int>> dictionary;
        private FileWriter() {  }
        public FileWriter(string path, ConcurrentDictionary<string, BlockingCollection<int>> dictionary)
        {
            this.path = path;
            this.dictionary = dictionary;
        }
        public void WriteToFile()
        {
            Console.WriteLine($"Writing to file start");
            try
            {
                using (StreamWriter writer = new StreamWriter(path, false, Encoding.Default))
                {
                    foreach(KeyValuePair<string, BlockingCollection<int>> pair in dictionary)
                    {
                        foreach(int item in pair.Value)
                        {
                            writer.Write(item);
                        }
                        writer.Write("\r\n"); //moving to the next string
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"File writer exception: {ex.Message}");
            }
            

            Console.WriteLine($"Writing to file end");

        }
    }
}
