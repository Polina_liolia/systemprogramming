﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _02_ThreadsAndFiles
{
    public class DataWriter
    {
        private static object locker = new object();
        public string Path { get; set; }
        public int LineNumber { get; set; }
        public string Data { get; set; }
        public int Count { get; set; }
        public DataWriter(string path, int line_number, string data, int count)
        {
            Path = path;
            LineNumber = line_number;
            Data = data;
            Count = count;
        }

        public void WriteData()
        {
            for (int j = 0; j < Count; j++)
            {
                string[] strings = new string[3];
                lock (locker)
                {
                    if (!File.Exists(Path))
                        File.Create(Path);
                    using (StreamReader reader = new StreamReader(new FileStream(Path, FileMode.Open), Encoding.Default))
                    {
                        int i = 0;
                        while (!reader.EndOfStream)
                        {
                            strings[i++] = reader.ReadLine();
                        }
                    }

                    using (FileStream file = new FileStream(Path, FileMode.Open))
                    {
                        int lineNumber = LineNumber - 1;
                        strings[lineNumber] = strings[lineNumber].Insert(strings[lineNumber].Length, Data);
                        file.Seek(0, SeekOrigin.Begin);

                        using (StreamWriter writer = new StreamWriter(file, Encoding.Default))
                        {
                            foreach (string s in strings)
                                writer.WriteLine(s);
                        }
                    }
                }
            }
        }
    }
}
