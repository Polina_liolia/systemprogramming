﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ThreadsAndFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"../../../output2.txt";
            DataWriter dw1 = new DataWriter(path, 1, "1", 5);
            DataWriter dw2 = new DataWriter(path, 2, "2", 5);
            DataWriter dw3 = new DataWriter(path, 3, "3", 5);
            Task task1 = new Task(dw1.WriteData);
            task1.Start();
            
            Task task2 = new Task(dw2.WriteData);
            task2.Start();
            
            Task task3 = new Task(dw3.WriteData);
            task3.Start();

            Task.WaitAll(task1, task2, task3);

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
