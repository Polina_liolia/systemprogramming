﻿using System;

namespace FootballMatch
{
    public class Fan
    {
        private int sectorNumber;
        private int seatNumber;
        private bool hasTicket;

        #region Public properties
        public int SectorNumber
        {
            get
            {
                return sectorNumber;
            }

            set
            {
                sectorNumber = value;
            }
        }

        public int SeatNumber
        {
            get
            {
                return seatNumber;
            }

            set
            {
                seatNumber = value;
            }
        }

        public bool HasTicket
        {
            get
            {
                return hasTicket;
            }

            set
            {
                hasTicket = value;
            }
        }
        #endregion

        private Fan() {}
        public Fan(bool hasTicket, int sectorNumber = -1, int seatNumber = -1)
        {
            HasTicket = hasTicket;
            if (HasTicket && (sectorNumber <= 0 || seatNumber <= 0))
                throw new ArgumentException("Wrong sector or seat number");
            SeatNumber = seatNumber;
            if (HasTicket && (SeatNumber <= 0 || SeatNumber > 10000))
                throw new ArgumentOutOfRangeException("Unexisting seat number");
            SectorNumber = sectorNumber;
            if (HasTicket && (SectorNumber <= 0 || SectorNumber > 6))
                throw new ArgumentOutOfRangeException("Unexisting sector number");
        }
    }
}