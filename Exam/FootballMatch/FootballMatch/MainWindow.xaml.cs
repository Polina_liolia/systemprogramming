﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoggerLib;

namespace FootballMatch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Private fields
        private static int totalSeatsOnStadion;
        private static int totalSeatsOccupied;
        private static int totalSeatsAvailable;

        private static int incomingFans;
        private static int outgoingFans;
        private static int fansWithoutTickets;

        private static int totalSeatsInSector1;
        private static int seatsOccupiedSector1;
        private static int seatsAvailableSector1;

        private static int totalSeatsInSector2;
        private static int seatsOccupiedSector2;
        private static int seatsAvailableSector2;

        private static int totalSeatsInSector3;
        private static int seatsOccupiedSector3;
        private static int seatsAvailableSector3;

        private static int totalSeatsInSector4;
        private static int seatsOccupiedSector4;
        private static int seatsAvailableSector4;

        private static int totalSeatsInSector5;
        private static int seatsOccupiedSector5;
        private static int seatsAvailableSector5;

        private static int totalSeatsInSector6;
        private static int seatsOccupiedSector6;
        private static int seatsAvailableSector6;
        #endregion

        #region Public properties with change notifications
        public int TotalSeatsOnStadion
        {
            get
            {
                return totalSeatsOnStadion;
            }

            set
            {
                totalSeatsOnStadion = value;
                this.NotifyProertyChanged("TotalSeatsOnStadion");
            }
        }

        public int TotalSeatsOccupied
        {
            get
            {
                return totalSeatsOccupied;
            }

            set
            {
                totalSeatsOccupied = value;
                this.NotifyProertyChanged("TotalSeatsOccupied");
            }
        }

        public int TotalSeatsAvailable
        {
            get
            {
                return totalSeatsAvailable;
            }

            set
            {
                totalSeatsAvailable = value;
                this.NotifyProertyChanged("TotalSeatsAvailable");
            }
        }

        public int IncomingFans
        {
            get
            {
                return incomingFans;
            }

            set
            {
                incomingFans = value;
                this.NotifyProertyChanged("IncomingFans");
            }
        }

        public int OutgoingFans
        {
            get
            {
                return outgoingFans;
            }

            set
            {
                outgoingFans = value;
                this.NotifyProertyChanged("OutgoingFans");
            }
        }

        public int FansWithoutTickets
        {
            get
            {
                return fansWithoutTickets;
            }

            set
            {
                fansWithoutTickets = value;
                this.NotifyProertyChanged("FansWithoutTickets");
            }
        }

        public int TotalSeatsInSector1
        {
            get
            {
                return totalSeatsInSector1;
            }

            set
            {
                totalSeatsInSector1 = value;
                this.NotifyProertyChanged("TotalSeatsInSector1");
            }
        }

        public int SeatsOccupiedSector1
        {
            get
            {
                return seatsOccupiedSector1;
            }

            set
            {
                seatsOccupiedSector1 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector1");
            }
        }

        public int SeatsAvailableSector1
        {
            get
            {
                return seatsAvailableSector1;
            }

            set
            {
                seatsAvailableSector1 = value;
                this.NotifyProertyChanged("SeatsAvailableSector1");
            }
        }

        public int TotalSeatsInSector2
        {
            get
            {
                return totalSeatsInSector2;
            }

            set
            {
                totalSeatsInSector2 = value;
                this.NotifyProertyChanged("TotalSeatsInSector2");
            }
        }

        public int SeatsOccupiedSector2
        {
            get
            {
                return seatsOccupiedSector2;
            }

            set
            {
                seatsOccupiedSector2 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector2");
            }
        }

        public int SeatsAvailableSector2
        {
            get
            {
                return seatsAvailableSector2;
            }

            set
            {
                seatsAvailableSector2 = value;
                this.NotifyProertyChanged("SeatsAvailableSector2");
            }
        }

        public int TotalSeatsInSector3
        {
            get
            {
                return totalSeatsInSector3;
            }

            set
            {
                totalSeatsInSector3 = value;
                this.NotifyProertyChanged("TotalSeatsInSecto3");
            }
        }

        public int SeatsOccupiedSector3
        {
            get
            {
                return seatsOccupiedSector3;
            }

            set
            {
                seatsOccupiedSector3 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector3");
            }
        }

        public int SeatsAvailableSector3
        {
            get
            {
                return seatsAvailableSector3;
            }

            set
            {
                seatsAvailableSector3 = value;
                this.NotifyProertyChanged("SeatsAvailableSector3");
            }
        }

        public int TotalSeatsInSector4
        {
            get
            {
                return totalSeatsInSector4;
            }

            set
            {
                totalSeatsInSector4 = value;
                this.NotifyProertyChanged("TotalSeatsInSector4");
            }
        }

        public int SeatsOccupiedSector4
        {
            get
            {
                return seatsOccupiedSector4;
            }

            set
            {
                seatsOccupiedSector4 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector4");
            }
        }

        public int SeatsAvailableSector4
        {
            get
            {
                return seatsAvailableSector4;
            }

            set
            {
                seatsAvailableSector4 = value;
                this.NotifyProertyChanged("SeatsAvailableSector4");
            }
        }

        public int TotalSeatsInSector5
        {
            get
            {
                return totalSeatsInSector5;
            }

            set
            {
                totalSeatsInSector5 = value;
                this.NotifyProertyChanged("TotalSeatsInSector5");
            }
        }

        public int SeatsOccupiedSector5
        {
            get
            {
                return seatsOccupiedSector5;
            }

            set
            {
                seatsOccupiedSector5 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector5");
            }
        }

        public int SeatsAvailableSector5
        {
            get
            {
                return seatsAvailableSector5;
            }

            set
            {
                seatsAvailableSector5 = value;
                this.NotifyProertyChanged("SeatsAvailableSector5");
            }
        }

        public int TotalSeatsInSector6
        {
            get
            {
                return totalSeatsInSector6;
            }

            set
            {
                totalSeatsInSector6 = value;
                this.NotifyProertyChanged("TotalSeatsInSector6");
            }
        }

        public int SeatsOccupiedSector6
        {
            get
            {
                return seatsOccupiedSector6;
            }

            set
            {
                seatsOccupiedSector6 = value;
                this.NotifyProertyChanged("SeatsOccupiedSector6");
            }
        }

        public int SeatsAvailableSector6
        {
            get
            {
                return seatsAvailableSector6;
            }

            set
            {
                seatsAvailableSector6 = value;
                this.NotifyProertyChanged("SeatsAvailableSector6");
            }
        }
        #endregion

        #region BlockingCollections
        private BlockingCollection<Fan> allFans;
        private BlockingCollection<Fan> queueToStadion;
        private BlockingCollection<Fan> queueToSectors;

        private BlockingCollection<Fan> seatsSector1;
        private BlockingCollection<Fan> seatsSector2;
        private BlockingCollection<Fan> seatsSector3;
        private BlockingCollection<Fan> seatsSector4;
        private BlockingCollection<Fan> seatsSector5;
        private BlockingCollection<Fan> seatsSector6;
        #endregion

        #region Service private fields
        private MyTxtLogger logger;
        private static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private static SynchronizationContext sync = SynchronizationContext.Current;
        private static Random random = new Random();
        Timer timer;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            //logger settings (every program call truncates log file):
            logger = MyTxtLogger.CreateTxtLogger(false);
            logger.Use_D = true;
            logger.WriteProtocol("Start", "MainWindow ctor", $"TaskID: {Task.CurrentId}");
            logger.WriteProtocol("Logger created", "MainWindow ctor", $"TaskID: {Task.CurrentId}");
            //setting data context for all UI bindings:
            this.DataContext = this;
            //static data:
            TotalSeatsOnStadion = 60000;
            TotalSeatsAvailable = TotalSeatsOnStadion;
            TotalSeatsOccupied = 0;
            OutgoingFans = 0;
            IncomingFans = 0;
            FansWithoutTickets = 0;
            TotalSeatsInSector1 = TotalSeatsInSector2 = TotalSeatsInSector3 = TotalSeatsInSector4 =
                TotalSeatsInSector5 = TotalSeatsInSector6 = TotalSeatsOnStadion / 6;
            SeatsAvailableSector1 = SeatsAvailableSector2 = SeatsAvailableSector3 = SeatsAvailableSector4 =
                SeatsAvailableSector5 = SeatsAvailableSector6 = TotalSeatsOnStadion / 6;
            seatsOccupiedSector1 = SeatsOccupiedSector2 = SeatsOccupiedSector3 = SeatsOccupiedSector4 =
                SeatsOccupiedSector5 = SeatsOccupiedSector6 = 0;
            //initializing a blocking collection for every sector:
            seatsSector1 = new BlockingCollection<Fan>(totalSeatsInSector1);
            seatsSector2 = new BlockingCollection<Fan>(totalSeatsInSector2);
            seatsSector3 = new BlockingCollection<Fan>(totalSeatsInSector3);
            seatsSector4 = new BlockingCollection<Fan>(totalSeatsInSector4);
            seatsSector5 = new BlockingCollection<Fan>(totalSeatsInSector5);
            seatsSector6 = new BlockingCollection<Fan>(totalSeatsInSector6);
            logger.WriteProtocol("End", "MainWindow ctor", $"TaskID: {Task.CurrentId}");
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyProertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion

        #region Events handlers
        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            logger.WriteProtocol("Event fired", "btn_Start_Click", $"TaskID: {Task.CurrentId}");
            Start();
        }

        private void btn_Stop_Click(object sender, RoutedEventArgs e)
        {
            logger.WriteProtocol("Event fired", "btn_Stop_Click", $"TaskID: {Task.CurrentId}");
            cancellationTokenSource.Cancel();
            if (timer != null)
                timer.Change(0, 0);
            logger.WriteProtocol("Timer stoped", "btn_Stop_Click", $"TaskID: {Task.CurrentId}");
        }

        private void btn_MatchEnd_Click(object sender, RoutedEventArgs e)
        {
            logger.WriteProtocol("Event fired", "btn_MatchEnd_Click", $"TaskID: {Task.CurrentId}");
            cancellationTokenSource.Cancel();
            if (timer != null)
                timer.Change(0, 0);
            logger.WriteProtocol("Timer stoped", "btn_Stop_Click", $"TaskID: {Task.CurrentId}");
            MatchEnded();
        }
        #endregion

        #region Start methods
        private void Start()
        {
            logger.WriteProtocol("Started", "Start", $"TaskID: {Task.CurrentId}");
            //all queues initialization:
            if (queueToStadion == null)
                queueToStadion = new BlockingCollection<Fan>(new ConcurrentQueue<Fan>(), TotalSeatsOnStadion);
            if (queueToSectors == null)
                queueToSectors = new BlockingCollection<Fan>(TotalSeatsOnStadion);

            //creating concurrent bag, containing all fans
            if (allFans == null)
            {
                allFans = new BlockingCollection<Fan>(new ConcurrentBag<Fan>());
                Task taskGenerateFans = CreateAllFansBag();
                taskGenerateFans.Wait();
            }

            //fans are comming to stadion (adding fans into queue to stadion):
            CreateStadionQueue();

            //fans having tickets are moving to sectors queue:
            MoveFansToSectorsQueue();

            //moving fans to their sectors:
            Task[] tasksMovingFans = new Task[]
            {
                Task.Factory.StartNew(() => MoveFansToSector(1)),
                Task.Factory.StartNew(() => MoveFansToSector(2)),
                Task.Factory.StartNew(() => MoveFansToSector(3)),
                Task.Factory.StartNew(() => MoveFansToSector(4)),
                Task.Factory.StartNew(() => MoveFansToSector(5)),
                Task.Factory.StartNew(() => MoveFansToSector(6)),
            };

            SetFansOutgoingTimer();
            
            //Task.WaitAll(tasksMovingFans);
            logger.WriteProtocol("Ended", "Start", $"TaskID: {Task.CurrentId}");
        }

        private Task CreateAllFansBag()
        {
            return Task.Factory.StartNew(() =>
           {
               logger.WriteProtocol("Started", "CreateAllFansBag", $"TaskID: {Task.CurrentId}");
               Task[] tasks = new Task[]
               {
                   Task.Factory.StartNew(() => CreateFansForSector(1)),
                   Task.Factory.StartNew(() => CreateFansForSector(2)),
                   Task.Factory.StartNew(() => CreateFansForSector(3)),
                   Task.Factory.StartNew(() => CreateFansForSector(4)),
                   Task.Factory.StartNew(() => CreateFansForSector(5)),
                   Task.Factory.StartNew(() => CreateFansForSector(6)),
               };
               Task.WaitAll(tasks);
               logger.WriteProtocol("Ended", "CreateAllFansBag", $"TaskID: {Task.CurrentId}");
           });

        }

        private void CreateFansForSector(int sector)
        {
            logger.WriteProtocol("Started", $"CreateFansForSector {sector}", $"TaskID: {Task.CurrentId}");
            //sector number validation:
            if (sector <= 0 || sector > 6)
            {
                logger.WriteProtocol($"ArgumentOutOfRangeException thrown (Wrong sector number{sector})", $"CreateFansForSector {sector}", $"TaskID: {Task.CurrentId}");
                throw new ArgumentOutOfRangeException("Wrong sector number");
            }
                
            for (int seat = 1; seat <= 10000;)
            {
                if (!allFans.IsCompleted)
                {
                    //random define if new fan has a ticket:
                    bool hasTicket = random.Next(0, 100) < 90 ? true : false;
                    Fan fan;
                    if (hasTicket) //creating new fan with valid ticket to current sector
                    {
                        fan = new Fan(hasTicket, sector, seat);
                        seat++;//counting only fans with valid ticket
                    }
                    else //creating new fan with valid ticket to current sector
                        fan = new Fan(hasTicket);
                    try
                    {
                        allFans.Add(fan, cancellationTokenSource.Token); //adding fan to concurrent bag
                    }
                    catch (OperationCanceledException ex)
                    {
                        logger.WriteProtocol($"Ecxeption catched: {ex.Message}", $"CreateFansForSector {sector}", $"TaskID: {Task.CurrentId}");
                        Debug.WriteLine(ex.Message);
                        return;
                    }
                }
            }
            logger.WriteProtocol("Ended", $"CreateFansForSector {sector}", $"TaskID: {Task.CurrentId}");
        }

        private Task CreateStadionQueue()
        {
            return Task.Factory.StartNew(() =>
            {
                logger.WriteProtocol("Started", "CreateStadionQueue", $"TaskID: {Task.CurrentId}");
                while (!allFans.IsCompleted)
                {
                    try
                    {
                        Fan fan = allFans.Take(cancellationTokenSource.Token);
                        queueToStadion.Add(fan, cancellationTokenSource.Token);
                    }
                    catch (OperationCanceledException ex)
                    {
                        logger.WriteProtocol($"Ecxeption catched: {ex.Message}", "CreateStadionQueue", $"TaskID: {Task.CurrentId}");
                        Debug.WriteLine(ex.Message);
                        break;
                    }
                    //queueToStadion.CompleteAdding();
                }
                logger.WriteProtocol("Ended", "CreateStadionQueue", $"TaskID: {Task.CurrentId}");
            });
        }


        private Task MoveFansToSectorsQueue()
        {
            return Task.Factory.StartNew(() =>
            {
                logger.WriteProtocol("Started", "MoveFansToSectorsQueue", $"TaskID: {Task.CurrentId}");
                while (!queueToStadion.IsCompleted)
                {
                    try
                    {
                        Fan fan = queueToStadion.Take(cancellationTokenSource.Token);
                        //if consumer has got a fan that has a ticket, moving him to the queue to sectors:
                        if (fan.HasTicket)
                        {
                            queueToSectors.Add(fan, cancellationTokenSource.Token);
                            IncomingFans++;
                        }
                        else
                        {
                            FansWithoutTickets++;
                        }
                    }
                    catch (OperationCanceledException ex)
                    {
                        logger.WriteProtocol($"Ecxeption catched: {ex.Message}", "MoveFansToSectorsQueue", $"TaskID: {Task.CurrentId}");
                        Debug.WriteLine(ex.Message);
                        break;
                    }
                }
                logger.WriteProtocol("Ended", "MoveFansToSectorsQueue", $"TaskID: {Task.CurrentId}");
            });
        }

        private void MoveFansToSector(int sector)
        {
            logger.WriteProtocol("Started", $"MoveFansToSector {sector}", $"TaskID: {Task.CurrentId}");
            if (sector <= 0 || sector > 6)
            {
                logger.WriteProtocol("ArgumentOutOfRangeException thrown (Wrong sector number)", $"MoveFansToSector {sector}", $"TaskID: {Task.CurrentId}");
                throw new ArgumentOutOfRangeException("Wrong sector number!");
            }
            BlockingCollection<Fan> sectorCollection = new BlockingCollection<Fan>();//default
            ListBox sectorListBox = lb_Sector1; //default
            #region Switching sector to initialize sectorCollection and sectorListBox
            switch (sector)
            {
                case 1:
                    {
                        sectorCollection = seatsSector1;
                        sectorListBox = lb_Sector1;
                        break;
                    }
                case 2:
                    {
                        sectorCollection = seatsSector2;
                        sectorListBox = lb_Sector2;
                        break;
                    }
                case 3:
                    {
                        sectorCollection = seatsSector3;
                        sectorListBox = lb_Sector3;
                        break;
                    }
                case 4:
                    {
                        sectorCollection = seatsSector4;
                        sectorListBox = lb_Sector4;
                        break;
                    }
                case 5:
                    {
                        sectorCollection = seatsSector5;
                        sectorListBox = lb_Sector5;
                        break;
                    }
                case 6:
                    {
                        sectorCollection = seatsSector6;
                        sectorListBox = lb_Sector6;
                        break;
                    }
            }
            #endregion
            while (!queueToSectors.IsCompleted )
            {
                try
                {
                    Fan fan = queueToSectors.Take(cancellationTokenSource.Token);
                    if (fan.SectorNumber == sector) //fan taken has a ticket to current sector
                    {
                        sectorCollection.Add(fan);
                        sync.Send(
                                  state => { sectorListBox.Items.Add(fan.SeatNumber); },
                                  null
                                  );
                        #region Changing static data:
                        TotalSeatsOccupied++;
                        TotalSeatsAvailable--;
                        switch(sector)
                        {
                            case 1:
                                {
                                    SeatsAvailableSector1--;
                                    SeatsOccupiedSector1++;
                                    break;
                                }
                            case 2:
                                {
                                    SeatsAvailableSector2--;
                                    SeatsOccupiedSector2++;
                                    break;
                                }
                            case 3:
                                {
                                    SeatsAvailableSector3--;
                                    SeatsOccupiedSector3++;
                                    break;
                                }
                            case 4:
                                {
                                    SeatsAvailableSector4--;
                                    SeatsOccupiedSector4++;
                                    break;
                                }
                            case 5:
                                {
                                    SeatsAvailableSector5--;
                                    SeatsOccupiedSector5++;
                                    break;
                                }
                            case 6:
                                {
                                    SeatsAvailableSector6--;
                                    SeatsOccupiedSector6++;
                                    break;
                                }
                        }
                        #endregion
                    }
                    else //fan taken has a ticket to another sector, so he has to be returned to sectors queue
                    {
                        queueToSectors.Add(fan, cancellationTokenSource.Token);
                    }
                }
                catch (OperationCanceledException ex)
                {
                    logger.WriteProtocol($"Exception catched: {ex.Message}", $"MoveFansToSector {sector}", $"TaskID: {Task.CurrentId}");
                    Debug.WriteLine(ex.Message);
                    break;
                }
            }
            logger.WriteProtocol("Ended", $"MoveFansToSector {sector}", $"TaskID: {Task.CurrentId}");
        }
        #endregion

        #region Fans outgoing
        //to call as taskcallback
        private void SetFansOutgoingTimer(Task task = null)
        {
            TimerCallback tm = new TimerCallback(FanGoesOut);
            int sector_number = random.Next(1, 7);
            // initializing timer:
            timer = new Timer(tm, //TimerCallback
                sector_number, //param
                random.Next(25000, 100000), //first firing delay
                random.Next(25000, 100000) //frequency
                );
        }


        //timer callback:
        private void FanGoesOut(object obj)
        {
            int sector_number = Convert.ToInt32(obj);
            if (sector_number <= 0 || sector_number > 6)
            {
                logger.WriteProtocol("ArgumentOutOfRangeException thrown: Wrong sector number", "FanGoesOut", $"TaskID: {Task.CurrentId}");
                throw new ArgumentOutOfRangeException("Wrong sector number");
            }
            logger.WriteProtocol($"Goes out from sector {sector_number}", "FanGoesOut", $"TaskID: {Task.CurrentId}");
            BlockingCollection<Fan> sectorCollection = null;
            ListBox sectorListBox = null;
            #region Switching sector to initialize sectorCollection and sectorListBox
            switch (sector_number)
            {
                case 1:
                    {
                        sectorCollection = seatsSector1;
                        sectorListBox = lb_Sector1;
                        break;
                    }
                case 2:
                    {
                        sectorCollection = seatsSector2;
                        sectorListBox = lb_Sector2;
                        break;
                    }
                case 3:
                    {
                        sectorCollection = seatsSector3;
                        sectorListBox = lb_Sector3;
                        break;
                    }
                case 4:
                    {
                        sectorCollection = seatsSector4;
                        sectorListBox = lb_Sector4;
                        break;
                    }
                case 5:
                    {
                        sectorCollection = seatsSector5;
                        sectorListBox = lb_Sector5;
                        break;
                    }
                case 6:
                    {
                        sectorCollection = seatsSector6;
                        sectorListBox = lb_Sector6;
                        break;
                    }
            }
            #endregion
            if (sectorCollection != null)
            {
                try
                {
                    Fan fan;
                    bool taken = sectorCollection.TryTake(out fan, 20, cancellationTokenSource.Token);
                    if (taken)
                    {
                        sync.Send(
                            (sender) =>
                            {
                                int fans_index = sectorListBox.Items.IndexOf(fan.SeatNumber);
                                sectorListBox.Items.RemoveAt(fans_index);
                            },
                            null);
                        #region Changing static data:
                        TotalSeatsOccupied--;
                        TotalSeatsAvailable++;
                        OutgoingFans++;
                        switch (sector_number)
                        {
                            case 1:
                                {
                                    SeatsAvailableSector1++;
                                    SeatsOccupiedSector1--;
                                    break;
                                }
                            case 2:
                                {
                                    SeatsAvailableSector2++;
                                    SeatsOccupiedSector2--;
                                    break;
                                }
                            case 3:
                                {
                                    SeatsAvailableSector3++;
                                    SeatsOccupiedSector3--;
                                    break;
                                }
                            case 4:
                                {
                                    SeatsAvailableSector4++;
                                    SeatsOccupiedSector4--;
                                    break;
                                }
                            case 5:
                                {
                                    SeatsAvailableSector5++;
                                    SeatsOccupiedSector5--;
                                    break;
                                }
                            case 6:
                                {
                                    SeatsAvailableSector6++;
                                    SeatsOccupiedSector6--;
                                    break;
                                }
                        }
                        #endregion
                    }
                }
                catch (OperationCanceledException ex)
                {
                    logger.WriteProtocol($"Exception catched: {ex.Message}", $"FanGoesOut sector {sector_number}", $"TaskID: {Task.CurrentId}");
                    Debug.WriteLine(ex.Message);
                }
            }
            //changing timer params:
            timer.Change(random.Next(25000, 100000), random.Next(25000, 100000));
        }

        private Task[] MatchEnded()
        {
            return new Task[]
            {
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(1)),
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(2)),
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(3)),
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(4)),
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(5)),
                Task.Factory.StartNew(()=> AllFansGoesOutFromSector(6)),
            };
        }
        private void AllFansGoesOutFromSector(int sector)
        {
            logger.WriteProtocol("Started", $"AllFansGoesOutFromSector {sector}", $"TaskID: {Task.CurrentId}");
            if (sector <= 0 || sector > 6)
            {
                logger.WriteProtocol("ArgumentOutOfRangeException thrown (Wrong sector number)", $"AllFansGoesOutFromSector {sector}", $"TaskID: {Task.CurrentId}");
                throw new ArgumentOutOfRangeException("Wrong sector number!");
            }
            BlockingCollection<Fan> sectorCollection = null;
            ListBox sectorListBox = null;
            #region Switching sector to initialize sectorCollection and sectorListBox
            switch (sector)
            {
                case 1:
                    {
                        sectorCollection = seatsSector1;
                        sectorListBox = lb_Sector1;
                        break;
                    }
                case 2:
                    {
                        sectorCollection = seatsSector2;
                        sectorListBox = lb_Sector2;
                        break;
                    }
                case 3:
                    {
                        sectorCollection = seatsSector3;
                        sectorListBox = lb_Sector3;
                        break;
                    }
                case 4:
                    {
                        sectorCollection = seatsSector4;
                        sectorListBox = lb_Sector4;
                        break;
                    }
                case 5:
                    {
                        sectorCollection = seatsSector5;
                        sectorListBox = lb_Sector5;
                        break;
                    }
                case 6:
                    {
                        sectorCollection = seatsSector6;
                        sectorListBox = lb_Sector6;
                        break;
                    }
            }
            #endregion
            if (sectorCollection != null)
            {
                while (!sectorCollection.IsCompleted)
                {
                    try
                    {
                        Fan fan = sectorCollection.Take();
                        sync.Send(
                            (sender) =>
                            {
                                int fans_index = sectorListBox.Items.IndexOf(fan.SeatNumber);
                                sectorListBox.Items.RemoveAt(fans_index);
                            },
                            null);
                        #region Changing static data:
                        TotalSeatsOccupied--;
                        TotalSeatsAvailable++;
                        OutgoingFans++;
                        switch (sector)
                        {
                            case 1:
                                {
                                    SeatsAvailableSector1++;
                                    SeatsOccupiedSector1--;
                                    break;
                                }
                            case 2:
                                {
                                    SeatsAvailableSector2++;
                                    SeatsOccupiedSector2--;
                                    break;
                                }
                            case 3:
                                {
                                    SeatsAvailableSector3++;
                                    SeatsOccupiedSector3--;
                                    break;
                                }
                            case 4:
                                {
                                    SeatsAvailableSector4++;
                                    SeatsOccupiedSector4--;
                                    break;
                                }
                            case 5:
                                {
                                    SeatsAvailableSector5++;
                                    SeatsOccupiedSector5--;
                                    break;
                                }
                            case 6:
                                {
                                    SeatsAvailableSector6++;
                                    SeatsOccupiedSector6--;
                                    break;
                                }
                        }
                        #endregion
                    }
                    catch (OperationCanceledException ex)
                    {
                        logger.WriteProtocol($"Exception catched: {ex.Message}", $"ArgumentOutOfRangeException sector {sector}", $"TaskID: {Task.CurrentId}");
                        Debug.WriteLine(ex.Message);
                    }
                }
            }
            logger.WriteProtocol("Ended", $"AllFansGoesOutFromSector {sector}", $"TaskID: {Task.CurrentId}");
        }
        #endregion

        
    }
}
