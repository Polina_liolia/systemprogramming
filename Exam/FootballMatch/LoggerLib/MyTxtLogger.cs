﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LoggerLib
{
    public class MyTxtLogger : IDisposable
    {
        private List<string> logsNames;
        private StreamWriter sw;
        private bool use_C;
        private bool use_D;
        private bool use_F;
        private bool use_E;
        private bool append;
        private object locker;

        #region Constructor
        private MyTxtLogger(bool append)
        {
            logsNames = new List<string>();
            logsNames.Add(@"c:\mylog.txt");
            logsNames.Add(@"d:\mylog.txt");
            logsNames.Add(@"e:\mylog.txt");
            logsNames.Add(@"f:\mylog.txt");
            Append = append;
            locker = new object();
        }
        #endregion

        #region Create logger
        public static MyTxtLogger CreateTxtLogger(bool append = true)
        {
            return new MyTxtLogger(append);
        }
        #endregion

        #region Public properties
        public bool Use_C
        {
            get
            {
                return use_C;
            }

            set
            {
                use_C = value;
                if (value)
                {
                    use_D = !value;
                    use_E = !value;
                    use_F = !value;
                    //trancating file if needed
                    if (!Append && File.Exists(this["C"]))
                        using (FileStream file = new FileStream(this["C"], FileMode.Truncate)) { }
                }
            }
        }

        public bool Use_D
        {
            get
            {
                return use_D;
            }

            set
            {
                use_D = value;
                if (value)
                {
                    use_C = !value;
                    use_E = !value;
                    use_F = !value;
                    //trancating file if needed
                    if (!Append && File.Exists(this["D"]))
                        using (FileStream file = new FileStream(this["D"], FileMode.Truncate)) { }
                }
            }
        }

        public bool Use_F
        {
            get
            {
                return use_F;
            }

            set
            {
                use_F = value;
                if (value)
                {
                    use_C = !value;
                    use_D = !value;
                    use_E = !value;
                    //trancating file if needed
                    if (!Append && File.Exists(this["F"]))
                        using (FileStream file = new FileStream(this["F"], FileMode.Truncate)) { }
                }
            }
        }

        public bool Use_E
        {
            get
            {
                return use_E;
            }

            set
            {
                use_E = value;
                if (value)
                {
                    use_C = !value;
                    use_D = !value;
                    use_F = !value;
                    //trancating file if needed
                    if (!Append && File.Exists(this["E"]))
                        using (FileStream file = new FileStream(this["E"], FileMode.Truncate)) { }
                }
            }
        }

        public bool Append
        {
            get
            {
                return append;
            }

            set
            {
                append = value;
            }
        }
        #endregion

        #region Indexators
        public string this[int index]
        {
            get { return logsNames[index]; }
        }

        public string this[string value]
        {
            get
            {
                if (value.Equals("C"))
                    return logsNames[0];
                if (value.Equals("D"))
                    return logsNames[1];
                if (value.Equals("E"))
                    return logsNames[2];
                if (value.Equals("F"))
                    return logsNames[3];
                else
                    throw new ArgumentOutOfRangeException("Such value was not found");
            }
        }
        #endregion

        #region WriteProtocol overloads
        //uses current log
        public void WriteProtocol(string action, string whoCalled, string description)
        {
            string currentLogName = use_C ? this["C"] :
                use_D ? this["D"] :
                use_E ? this["E"] : this["F"];
            try
            {
                lock (locker)
                {
                    using (sw = new StreamWriter(currentLogName, true, System.Text.Encoding.Default))
                    {
                        sw.Write(DateTime.Now.ToString());
                        sw.Write("\t");
                        sw.Write(action);
                        sw.Write("\t");
                        sw.Write(whoCalled);
                        sw.Write("\t");
                        sw.Write(description);
                        sw.Write("\r\n");
                        sw.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //uses directly pointed log
        public void WriteProtocol(string logName, string action, string whoCalled, string description)
        {
            try
            {
                lock (locker)
                {
                    using (StreamWriter sw = new StreamWriter(logName, true, System.Text.Encoding.Default))
                    {
                        sw.Write(DateTime.Now.ToString());
                        sw.Write("\t");
                        sw.Write(action);
                        sw.Write("\t");
                        sw.Write(whoCalled);
                        sw.Write("\t");
                        sw.Write(description);
                        sw.Write("\r\n");
                        sw.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region IDisposable
        public void Dispose()//for manual call
        {
            sw.Dispose();
        }
        void IDisposable.Dispose() //auto-called
        {
            sw.Dispose();
        }

        #endregion

    }
}
