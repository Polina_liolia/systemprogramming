﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibStudent
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public override string ToString()
        {
            return $"{LastName} {FirstName} {BirthDate.ToLongDateString()}";
        }

        //fake method for reflection tests:
        public int M1(int a, int b)
        {
            Console.WriteLine("Student fake method");
            return a + b;
        }
    }
}
