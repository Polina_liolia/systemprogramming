﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
//using LibStudent;

using System.IO;
using static System.Console;

namespace AssemblyReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            // определение текущей сборки
            Assembly theAssembly = Assembly.GetExecutingAssembly();

            // использование свойств и методов для текущей сборки
            WriteLine($"Assembly name:\n\t{theAssembly.FullName}\n");
            WriteLine($"Full path of the uploaded file:\n\t{theAssembly.Location}\n");
            WriteLine($"Whether the assembly was loaded into a context that is intended only for reflection:\n\t{theAssembly.ReflectionOnly}\n");
            WriteLine($"Originally specified assembly location:\n\t{theAssembly.CodeBase}\n");
            WriteLine($"Assembly entry point:\n\t{theAssembly.EntryPoint}");

            FileStream[] files = theAssembly.GetFiles(true);
            WriteLine($"\nNumber of files: {files.Length}");
            foreach (FileStream f in files)
            {
                WriteLine($"\t{f.Name}");
            }
            Module[] modules = theAssembly.GetLoadedModules(true);
            WriteLine($"\nNumber of modules: {modules.Length}");
            foreach (Module m in modules)
            {
                WriteLine($"\t{m.Name}");
            }
            Console.WriteLine("______________________________________");

            

            Assembly a = Assembly.Load("mscorlib");
            foreach (Type type in a.GetTypes())
            {
                if (!type.IsPrimitive)
                {
                    continue;
                }

                Write($"Type name: {type.Name}  ");
                if (type.BaseType != null)
                {
                    Write($"\tBase type name: {type.BaseType.Name}");
                }
                WriteLine();
            }
            Console.WriteLine("______________________________________");


            
            GetMethodsInfo();
            Console.WriteLine("______________________________________");


            //getting type of variable:
            int n = new int();
            Type t1 = Type.GetType("System.Int32",
                false, //don't throw exception if not fount (null will be assigned)
                true //ignore Case
                );
            Type t2 =  n.GetType();
            Console.WriteLine(t1);
            Console.WriteLine(t2);
            Console.WriteLine("______________________________________");

            Console.WriteLine("Refferenced dll reflection:"); 
            Assembly lib_asm = Assembly.Load("LibStudent");
            //getting loaded assembly name:
            Console.WriteLine($"Assembly name: {lib_asm.GetName().Name}.");
            //getting all types used in assembly:
            Type[] types = lib_asm.GetTypes();
            foreach(Type item in types)
                Console.WriteLine(item);
            Type t = lib_asm.GetType("LibStudent.Student");
            Console.WriteLine($"Type of class inside library: {t}");
            object obj = Activator.CreateInstance(t); //creating an instance of Studen class from loaded library
            MethodInfo method = t.GetMethod("M1"); //geting method M1 of class Student
            //calling method from loaded dll:
            int result = (int)method //method M1
                .Invoke( //calling method M1
                obj, //instanse of Student class
                new object[] { 53, 47 } //method parameters
                ); 
            Console.WriteLine($"Result of M1 method of Student class call: {result}");
            Console.WriteLine("______________________________________");
        
        }

        static void GetMethodsInfo()
        {
            Type t = typeof(Int32);
            //перечисление методов
            foreach (MethodInfo mi in t.GetMethods())
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                //вывод имени
                Console.Write(mi.Name);
                //вывод типа возврата
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write(" Returns {0} ", mi.ReturnType.Name);
                //вывод списка параметров
                if (mi.GetParameters().Length != 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Parameters: ");
                    foreach (ParameterInfo pi in mi.GetParameters())
                    {
                        Console.Write("{0} {1} ", pi.ParameterType.Name, pi.Name);
                        if (pi.IsIn) Console.Write("input ");
                        if (pi.IsRetval) Console.Write("retval");
                    }
                }
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
