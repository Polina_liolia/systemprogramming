﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using Scripting;

namespace ZippingTestInThread
{
    //
    #region Продолжение выполнения "тасков"- автоматический запуск новой задачи, после завершения первой задачи.
    class Program
    {
        //Метод который будет выполнен как задача : например архивирование файла
        //Передлать основіваясь на 09_TPL из ThreadTests в вариант, который принимает путь и имя файла как параметры
        static void MyTask()
        {
            Console.WriteLine("MyTask() запущен.");

            CompressedZipFileFromDirectory(@"..\..\..\Output\Compressed", @"..\..\..\Output\test.txt");

            Console.WriteLine("MyTask() завершен.");
        }

        //Метод исполняемый как продолжение задачи : отправка созданного архива по почте, фтп и т.п.
        static void ContinuationTask(Task task)
        {
            Console.WriteLine("Продолжение запущено.");
            FileSystemObject fso = new FileSystemObject();
            if (fso.FileExists(@"..\..\..\Output\Compressed\test.txt.ZIP"))
                System.Windows.Forms.MessageBox.Show("Архив создан и отправлен.");
            else
                System.Windows.Forms.MessageBox.Show("Ошибка в создании архива.");

            Console.WriteLine("Продолжение завершено.");
        }

        static void Main()
        {
            Console.WriteLine("Основной поток запущен.");

            // Создание экземпляра задачи.
            var action = new Action(MyTask);
            var task = new Task(action);

            // Создание продолжения задачи.
            var continuationAction = new Action<Task>(ContinuationTask);
            Task taskContinuation = task.ContinueWith(ContinuationTask);

            // Выполнение последовательности задач.
            task.Start();

            // Ожидание завершения продолжения. 
            taskContinuation.Wait();
            //task.Wait();
            task.Dispose();
            taskContinuation.Dispose();

            Console.WriteLine("Основной поток завершен.");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }

        public static bool CompressedZipFileFromDirectory(string namePathForCompressed, string nameFile)
        {
            bool result = true;
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(namePathForCompressed); // recurses subdirectories
                    zip.TempFileFolder = @"..\..\..\Output\tmpinfo";//Environment.CurrentDirectory;// @"c:\tmpinfo";//
                    zip.Save(nameFile + ".ZIP");
                }
            }
            catch (System.Exception ex1)
            {
                result = false;
                System.Console.Error.WriteLine("exception: " + ex1);
            }

            return result;
        }
    }
    #endregion
}
