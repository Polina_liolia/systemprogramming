﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using Scripting;

namespace ZippingTestInThread
{
    //
    #region Продолжение выполнения "тасков"- автоматический запуск новой задачи, после завершения первой задачи.
    class Program
    {
        
        static void Main()
        {
            Console.WriteLine("Основной поток запущен.");
            string path = @"..\..\..\Output\Compressed";
            string file = @"..\..\..\Output\test.txt";
            string tmpFolder = @"..\..\..\Output\tmpinfo";
            // Создание экземпляра задачи.
            Task task = new Task(() => MyTask(path, file, tmpFolder));

            // Создание продолжения задачи.
            var continuationAction = new Action<Task>(ContinuationTask);
            Task taskContinuation = task.ContinueWith(ContinuationTask);

            // Выполнение последовательности задач.
            task.Start();

            // Ожидание завершения продолжения. 
            taskContinuation.Wait();
            //task.Wait();
            task.Dispose();
            taskContinuation.Dispose();

            Console.WriteLine("Основной поток завершен.");

            // Задержка.
            Console.WriteLine("нажмите любую клавишу...");
            Console.ReadKey();
        }

        //Метод который будет выполнен как задача : например архивирование файла
        //Передлать основіваясь на 09_TPL из ThreadTests в вариант, который принимает путь и имя файла как параметры
        static void MyTask(string namePathForCompressed, string nameFile, string tempFileFolder)
        {
            Console.WriteLine("MyTask() запущен.");

            CompressedZipFileFromDirectory(namePathForCompressed, nameFile, tempFileFolder);

            Console.WriteLine("MyTask() завершен.");
        }

        //Метод исполняемый как продолжение задачи : отправка созданного архива по почте, фтп и т.п.
        static void ContinuationTask(Task task)
        {
            Console.WriteLine("Продолжение запущено.");
            FileSystemObject fso = new FileSystemObject();
            if (fso.FileExists(@"..\..\..\Output\test.txt.ZIP"))
                System.Windows.Forms.MessageBox.Show("Архив создан и отправлен.");
            else
                System.Windows.Forms.MessageBox.Show("Ошибка в создании архива.");

            Console.WriteLine("Продолжение завершено.");
        }


        public static bool CompressedZipFileFromDirectory(string namePathForCompressed, string nameFile, string tempFileFolder)
        {
            bool result = true;
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(namePathForCompressed); // recurses subdirectories
                    zip.TempFileFolder = tempFileFolder;//Environment.CurrentDirectory;// @"c:\tmpinfo";//
                    zip.Save(nameFile + ".ZIP");
                }
            }
            catch (System.Exception ex1)
            {
                result = false;
                System.Console.Error.WriteLine("exception: " + ex1);
            }

            return result;
        }
    }
    #endregion
}
