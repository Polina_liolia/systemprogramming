﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_Autoload
{
    class Program
    {
        public static object MessageBox { get; private set; }

        static void Main(string[] args)
        {
            //LOAd или конструктор
            try
            {
                // RegistryKey - Инициализация объекта для работы с веткой LocalMachine.
                RegistryKey regKey = Registry.LocalMachine;

                regKey = regKey.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                string Temp = (string)regKey.GetValue("ItStepSoftware", "");

                if (Temp != "")
                    label3.Text = "программа стоит в автозагрузке";
            }
            catch (Exception ex)
            {
                label3.Text = "программа не стоит в автозагрузке";
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //внесение
            try
            {
                RegistryKey regKey = Registry.LocalMachine;
                regKey = regKey.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");

                // Путь к данному приложению.
                regKey.SetValue("ItStepSoftware", Application.StartupPath + "\\WindowsFormsApplication2.exe");
                label3.Text = "программа стоит в автозагрузке";
            }
            catch (Exception ex)
            {
                label3.Text = "программа не стоит в автозагрузке";
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //
            try
            {
                RegistryKey regKey = Registry.LocalMachine;
                regKey = regKey.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                regKey.DeleteValue("ItStepSoftware");
                label3.Text = "программа не стоит в автозагрузке";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
