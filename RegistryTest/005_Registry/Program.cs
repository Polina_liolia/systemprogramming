﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_Registry
{
    // Редактирование реестра.
    class Program
    {
        static void Main()
        {
            // Совершаем навигацию в нужное место и открываем его для записи.
            RegistryKey myKey = Registry.CurrentUser;
            RegistryKey wKey = myKey.OpenSubKey("Software", true);
            RegistryKey newKey = wKey.CreateSubKey("ItStepSoftware");

            // Совершаем запись в реестр.
            newKey.SetValue("TheStringName", "I contain string value.");
            newKey.SetValue("TheInt32Name", 1234567890);

            // Тип можно указать явно.
            newKey.SetValue("AnotherName", 1234567890, RegistryValueKind.String);

            wKey.Close();
            newKey.Close();

            // Задержка на экране.
            // Console.ReadKey();
        }
    }
}
