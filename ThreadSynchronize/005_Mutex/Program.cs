﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Threading;

namespace _005_Mutex
{
    // Ииспользование Mutex для синхронизации доступа к защищенным ресурсам.

    // Mutex - Примитив синхронизации, который также может использоваться в межпроцессной и междоменной синхронизации.
    // MutEx - Mutual Exclusion (Взаимное Исключение).


    //

    class Program
    {
        // Mutex - Примитив синхронизации, который также может использоваться в межпроцессорной синхронизации.
        // функционирует аналогично AutoResetEvent но снабжен дополнительной логикой:
        // 1. Запоминает какой поток им владеет. ReleaseMutex не может вызвать поток, который не владеет мьютексом.
        // 2. Управляет рекурсивным счетчиком, указывающим, сколько раз поток-владелец уже владел объектом.
        private static readonly Mutex Mutex1 = new Mutex(false, "MutexSample:AAED7056-380D-412E-9608-763495211EA8");
        //два параметра - логический и имя мьютекса - гуид. Именованный мьютекс может использоваться из разных приложений
        //разных процессов - exe
        static void Main()
        {
            var threads = new Thread[5];

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(new ThreadStart(Function))
                {
                    Name = i.ToString()
                };
                threads[i].Start();
            }

            // Delay.
            Console.ReadKey();
        }

        static void Function()
        {
            bool myMutex = Mutex1.WaitOne();
            //"Остальные" будут ждать, пока Мьютекс не осободиться.
            Console.WriteLine("Поток {0} зашел в защищенную область.", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.WriteLine("Поток {0}  покинул защищенную область.\n", Thread.CurrentThread.Name);
            Mutex1.ReleaseMutex();
        }
    }
    #region Рекурсивное запирание: количество вызовов mutex.WaitOne(); должно = количству вызовов mutex.ReleaseMutex();
    /*
    class Program
    {
        static void Main()
        {
            var instance = new SomeClass();

            var thread = new Thread(instance.Method1);
            thread.Start();

            var thread2 = new Thread(instance.Method1);
            thread2.Start();

            // Delay.
            thread.Join();
            thread2.Join();
            Console.ReadKey();
        }
    }

    // Рекурсивное запирание.
    public class SomeClass : IDisposable
    {
        private Mutex mutex = new Mutex();

        public void Method1()
        {
            mutex.WaitOne();
            Console.WriteLine("Method1 Start "+Thread.CurrentThread.ManagedThreadId);
            Method2();
            mutex.ReleaseMutex();
            Console.WriteLine("Method1 End " + Thread.CurrentThread.ManagedThreadId);
        }

        public void Method2()
        {
            mutex.WaitOne();
     
            Console.WriteLine("Method2 Start " + Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(1000);
            mutex.ReleaseMutex();
            Console.WriteLine("Method2 End " + Thread.CurrentThread.ManagedThreadId);
        }

        public void Dispose()
        {
            mutex.Dispose();
        }
    }
    */
    #endregion

}
