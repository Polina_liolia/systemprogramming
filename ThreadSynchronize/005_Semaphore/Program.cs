﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _005_Semaphore
{
    // Класс Semaphore - используется для управления доступом к пулу ресурсов. 
    // Потоки занимают слот семафора, вызывая метод WaitOne(), и освобождают занятый слот вызовом метода Release().

    public class Program
    {
        private static Semaphore pool;

        private static void Work(object number)
        {
            pool.WaitOne();

            Console.WriteLine("Поток {0} занял слот семафора.", number);
            Thread.Sleep(1000);
            Console.WriteLine("Поток {0} -----> освободил слот.", number);

            pool.Release();
        }

        public static void Main()
        {
            // Первый аргумент:
            // Задаем количество слотов для использования в данный момент (не более максимального клоличества).
            // Второй аргумент:
            // Задаем максимальное количество слотов для данного семафора.
            pool = new Semaphore(2, //2 уже занято - выключено изначально. Значит к ресурсу получт доступ Макс - 2
                                 4,//всего 4 слота - значит к ресурсу максимум одновременно могут получить доступ 4 потока
                                 "MySemafore65487563487");

            for (int i = 1; i <= 8; i++)
            {
                Thread thread = new Thread(new ParameterizedThreadStart(Work));
                thread.Start(i);
            }
            Thread.Sleep(2000);
            pool.Release(2);

            // Задержка.
            Console.ReadKey();
        }
    }

}
