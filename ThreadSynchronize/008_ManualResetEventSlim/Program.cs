﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _008_ManualResetEventSlim
{
    class Program
    {
        // ManualResetEventSlim - изначально используется SpinWait блокировка на 1000 итераций, 
        // после чего происходит синхронизация с помощью объекта ядра.
        static ManualResetEventSlim slim = new ManualResetEventSlim(false, 1000);

        static void Main()
        {
            Thread[] threads = { new Thread(Function), new Thread(Function), new Thread(Function) };

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Name = i.ToString();
                threads[i].Start();
            }

            Console.ReadKey();
            slim.Set();

            // Delay.
            Console.ReadKey();
        }

        static void Function()
        {
            slim.Wait();
            Console.WriteLine("Поток {0} начал работу.", Thread.CurrentThread.Name);
        }
    }

}
