﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleOne_
{
    public class ClassOne : IAssemblyExample
    {
        public string SomeMethod(int n)
        {
            return $"ModuleOne: {n}";
        }
    }
}
