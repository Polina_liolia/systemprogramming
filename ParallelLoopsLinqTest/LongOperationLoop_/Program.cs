﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongOperationLoop_
{
    class Program
    {
        static void Main(string[] args)
        {
            long end = 100000;
            double[] massiv = new double[end + 1];
            {
                Console.WriteLine("Начало очень долгой операции ...");
                DateTime dt_start = DateTime.Now;
                string buf = "";
                // Используем стандартный цикл
                for (long i = 0; i <= end; i++)
                {
                    buf += i.ToString();
                }
                Console.WriteLine((DateTime.Now - dt_start).Seconds.ToString());
                // У меня было 23 секунд
                Console.WriteLine("Press any key ...");
                Console.ReadLine();
            }
            {
                DateTime dt_start = DateTime.Now;
                string buf = "";
                // Используем стандартный цикл
                Parallel.For(0, end + 1, index => { buf += index; });
                // Здесь было 3 секунды
                Console.WriteLine((DateTime.Now - dt_start).Seconds.ToString());
                Console.WriteLine("Press any key ...");
                Console.ReadLine();
            }
        }
    }
}
