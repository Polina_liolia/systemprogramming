﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelLoopsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new Action(() => Console.WriteLine($"First {Task.CurrentId}"));
            var b = new Action(() => Console.WriteLine($"Second {Task.CurrentId}"));
            var c = new Action(() => Console.WriteLine($"Third {Task.CurrentId}"));
            Console.WriteLine("Parallel Invoke");
            Parallel.Invoke(a, b, c);
            //this are blocking operations; wait on all tasks
            //Parallel применяется, чтобы распараллелить действия в циклах,
            //т.е. все итерации будут выполняться не последовательно, а параллельно
            //Примеры для статических методов For() и ForEach()
            Console.WriteLine("Parallel For");
            Parallel.For(1,//from inclusive
                        11,//to exclusive
                        x => //Action<int> body
            {
                Console.WriteLine($"{x*x}\t");
            });

            Console.WriteLine("Parallel ForEach");
            List<string> words = new List<string>() { "dog", "cat", "monkey", "bear", "pig" };
            Parallel.ForEach(words,//from inclusive
                           x => //Action<string> body
                            {
                               Console.WriteLine($"Word {x} in task {Task.CurrentId}\t");
                           });

            var po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 2;
            Parallel.ForEach(words, po, word =>
            {
                Console.WriteLine($"{word} has length {word.Length} (task {Task.CurrentId})");
            });

            Console.WriteLine("Parallel.ForEach && Range");
            // has a step strictly equal to 1
            // if you want something else...
            Parallel.ForEach(Range(1, 20, 3), Console.WriteLine);

            Console.WriteLine("Press any key ...");
            Console.ReadKey();

        }

        public static IEnumerable<int> Range(int start, int end, int step)
        {
            for (int i = start; i < end; i += step)
            {
                yield return i;
            }
        }

    }
}
