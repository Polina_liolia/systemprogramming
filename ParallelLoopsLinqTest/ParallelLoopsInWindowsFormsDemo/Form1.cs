﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParallelLoopsInWindowsFormsDemo
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnStartParallelTask_Click(object sender, EventArgs e)
        {
            //var a = new Action(() =>txtOutput.Text += $"First {Task.CurrentId}");
            //var b = new Action(() =>txtOutput.Text += $"Second {Task.CurrentId}");
            //var c = new Action(() =>txtOutput.Text += $"Third {Task.CurrentId}");
            #region Inviking delegates from main graphical thread
            var a = new Action(() =>
            {
                MainForm myForm = this;
                myForm.Invoke(new EventHandler (delegate
                {
                    txtOutput.Text += $"First {Task.CurrentId}\r\n";
                }));
            });
            var b = new Action(() => 
            this.Invoke(new EventHandler (delegate
            {
                txtOutput.Text += $"Second {Task.CurrentId}\r\n";
            })));
            var c = new Action(() => 
            this.Invoke(new EventHandler(delegate
            {
                txtOutput.Text += $"Third {Task.CurrentId}\r\n";
            })));
            #endregion
            Parallel.Invoke(a, b, c);
        }
    }
}
